package usyd.comp3615.tests;

import java.util.List;

import junit.framework.Assert;
import usyd.comp3615.edia.db.DatabaseHandler;
import usyd.comp3615.edia.db.Item;
import usyd.comp3615.edia.db.ItemTree;
import android.test.AndroidTestCase;

public class ItemTreeTest extends AndroidTestCase{
	DatabaseHandler dbHandler;
	ItemTree itemTree;
	
	public ItemTreeTest(){
		dbHandler = new DatabaseHandler(this.getContext());
		dbHandler.createDataBase();
		itemTree = ItemTree.getInstance();
		itemTree.init(dbHandler); //loads the contents of the db into the item tree
	}
	
	public void testAllItemsExist() throws Throwable{
		Assert.assertEquals(dbHandler.getAllItems().size(), itemTree.getSize());
	}
	
	public void testAllItemsFindable() throws Throwable{
		int numberFound = 0;
		List<Item> allItems = dbHandler.getAllItems();
		for (Item i : allItems){
			if (itemTree.findNode(i.getName()) != null){
				numberFound++;
			}
			else{
				System.err.println("Could not find node for item \"" + i.getName() + "\"");
			}
		}
		Assert.assertEquals(numberFound, allItems.size());
	}
}