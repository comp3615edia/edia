package usyd.comp3615.edia;

import java.util.concurrent.ExecutionException;

import usyd.comp3615.edia.db.DatabaseHandler;
import usyd.comp3615.edia.server.SendRegistration;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class ActivityLevel extends Activity {
	
	private String data[];
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.registration2);
		
		Bundle b = this.getIntent().getExtras();
		data = b.getStringArray("data");
		
	}
	
	public void onClick(View view) {
		Button b = (Button) view;
		String buttonText = b.getText().toString();
		
		try {
			// 0:name, 1:email, 2:gender, 3:weight, 4:height, 5:birth, 6:password, 7:confirmation
			connectToServer(data[0], data[1], data[2], data[3], data[4], data[5],
							data[6], data[7], buttonText, view);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		
	}
	
	public void connectToServer(String name, String email, String gender, String weight, 
			String height, String birth, String password, String confirmation, String activity, View view) throws InterruptedException, ExecutionException {
		// Send data to new thread in order to send to server
		String arr[] = {name, email, gender, weight, height, birth, password, confirmation, activity};
		SendRegistration sl = new SendRegistration();
		sl.execute(arr);
		if (sl.get()) {
			myToast("Successful Registration");
			saveLogin(email);
			switchMain(view);
		}
		else {
			myToast(sl.getMessage());
		}
	}
	
	private void saveLogin(String email) {
		DatabaseHandler db = new DatabaseHandler(this);
		db.login(email);
	}
	
	//A helper method to put a Toast object on the screen.
	public void myToast(String msg){
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
	}
	
	//Switch to the main screen.
	public void switchMain(View view){
		Intent intent = new Intent(ActivityLevel.this, MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	
}
