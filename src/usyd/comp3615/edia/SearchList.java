package usyd.comp3615.edia;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import usyd.comp3615.edia.db.CSVParser;
import usyd.comp3615.edia.db.Consumption;
import usyd.comp3615.edia.db.DatabaseHandler;
import usyd.comp3615.edia.db.Item;
import usyd.comp3615.edia.db.ItemTree;
import usyd.comp3615.edia.db.SearchResult;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;

public class SearchList extends Activity implements OnQueryTextListener {
	
	final String[] suffixKeywords = new String[]{"added ", "contains ", "no ", "with ", "sandwiched ", "coated ", "fried "};
	private final int RESULT_LIMIT = 50;
	private final int MIN_QUERY_LENGTH = 3;
	private final int NUM_DAYS_RECENT = 5; //number of days until an item is no longer considered recent
		
	private static String currentMealType = "breakfast";
	private static QuantitySelector.Mode mode;
	
	private DatabaseHandler db = new DatabaseHandler(this); 
	
	private List<String> listItems = new ArrayList<String>(); //the "short names" of the items (eg. peeled apple) which is shown in search results
	private HashMap<String, ArrayList<String>> associatedNodePaths = new HashMap<String, ArrayList<String>>(); // a single name in the list could be referring to several different items (eg. peeled apple could be red or green skin) so 
	private ArrayAdapter<String> adapter;
	
	private ItemTree itemTree = new ItemTree();
	private SearchView searchBar;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.searchpage);
		
		searchBar = (SearchView) findViewById(R.id.searchView1);
		searchBar.setOnQueryTextListener(this);

		Bundle b = this.getIntent().getExtras();
		currentMealType = b.getString("meal_type");
		mode = (QuantitySelector.Mode) b.getSerializable("mode");
		
		adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listItems);
		ListView listView = (ListView) findViewById(R.id.listView1);
		listView.setAdapter(adapter);
		
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				String item = ((TextView)view).getText().toString();
				ArrayList<String> associatedNodes = associatedNodePaths.get(listItems.get(position));		
				Bundle b = new Bundle();				
				b.putString("result", item);
				b.putString("meal_type", currentMealType);
				b.putSerializable("mode", mode);
				//ItemSelection is ADD mode, so no need to set
				b.putStringArrayList("nodeNames", associatedNodes);
				Intent intent = new Intent(SearchList.this, ItemSelection.class);
				intent.putExtras(b);
				startActivityForResult(intent, RequestCode.ITEM_SELECTION);
			}
		});
		
		itemTree = ItemTree.getInstance();
		searchBar.setQuery("", false); //start the search with an empty string (which will load recently used items)
		searchBar.requestFocus();
	}
	public boolean onQueryTextChange(String arg0) {
		associatedNodePaths.clear();
		listItems.clear();
		adapter.notifyDataSetChanged();

		if (arg0.length() < MIN_QUERY_LENGTH){
			//TODO: set seperator title to "Recently used items"
			//get all the consumptions from the last NUM_DAYS_RECENT days
			if (currentMealType != null){
				List<Consumption> recentConsumptions = new ArrayList<Consumption>();
				for (int i = 1; i < NUM_DAYS_RECENT; i++){
					recentConsumptions.addAll(db.getAllConsumptionItems(i, currentMealType));
				}
			
				//add them to our list, and their paths to the associatedNodePaths
				for (Consumption c : recentConsumptions){
					String displayedName = c.getShortName();
					Item relatedItem = db.getItem(c.getSurveyID());
					listItems.add(displayedName);
					ArrayList<String> nodePath = new ArrayList<String>();
					nodePath.add(relatedItem.getName().toLowerCase());
					associatedNodePaths.put(displayedName, nodePath);
				}
				Collections.sort(listItems);
				adapter.notifyDataSetChanged();
			}
			return false;
		}

		//TODO: set seperator title to "Search results"
		List<SearchResult> results = db.getNamesCal(arg0);
		for (SearchResult result : results){
			List<String> tiers = CSVParser.parse(result.getNodePath());

			//generate the string to be shown to the user, which is a simplified version of the items full name that only shows the sections relevant to the search
			List<String> suffices = new ArrayList<String>();
			result.getRelevantTiers().remove(0); // we don't want to show tier 0 twice, even if it does contain the search query
			String itemName = ""; 
			for (int relevantTier : result.getRelevantTiers()){
				String tierString = tiers.get(relevantTier);

				boolean isSuffix = false;

				for (String keyword : suffixKeywords){
					if (tierString.startsWith(keyword)){
						suffices.add(tierString);
						isSuffix = true;
						break;
					}
				}
				if (!isSuffix)
					itemName += tiers.get(relevantTier) + " ";
			}
			itemName += tiers.get(0);
			for (String suffix : suffices){
				itemName += ", " + suffix;
			}

			// find an appropriate spot for it in the search result list, which is sorted by index of search query with ties broken alphabetically		
			for (int i = 0; i != listItems.size()+1; ++i){ 	
				String firstSearchTerm = arg0.split(" ")[0];	
				if (i == listItems.size() || 
						itemName.indexOf(firstSearchTerm) < listItems.get(i).indexOf(firstSearchTerm) ||
						(itemName.indexOf(firstSearchTerm) == listItems.get(i).indexOf(firstSearchTerm) && itemName.compareTo(listItems.get(i)) <= 0)){
					if (!associatedNodePaths.containsKey(itemName)){
						listItems.add(i, itemName);
						associatedNodePaths.put(itemName, new ArrayList<String>());
					}
					associatedNodePaths.get(itemName).add(result.getNodePath());				
					break;
				}
			}
		}

		//take only the most relevant RESULT_LIMIT items and find the associated nodes
		listItems.subList(Math.min(listItems.size(), RESULT_LIMIT), listItems.size()).clear();

		adapter.notifyDataSetChanged();
		return false;
	}

	/**
	 * Catches the result of the QuantitySelection that is called when an item is selected, and then lets the calling process know that it was successful (returning information if required)
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		if (requestCode == RequestCode.ITEM_SELECTION){
			if (resultCode == RESULT_OK){
				Intent returnIntent = new Intent();
				if (mode == QuantitySelector.Mode.SELECT){ //if in select mode, we need to return some arguments to the caller
					returnIntent.putExtra("id", data.getIntExtra("id", 0));
					returnIntent.putExtra("short_name", data.getStringExtra("short_name"));
					returnIntent.putExtra("quantity", data.getIntExtra("quantity", 0));
				}
				setResult(RESULT_OK, returnIntent);
				finish();
			}
		}
	}
	
	@Override
	public boolean onQueryTextSubmit(String arg0) {
		// TODO Auto-generated method stub
		return false;
	}

}
