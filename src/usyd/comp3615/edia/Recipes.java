package usyd.comp3615.edia;

import java.util.ArrayList;
import java.util.HashSet;

import usyd.comp3615.edia.db.Consumption;
import usyd.comp3615.edia.db.DatabaseHandler;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class Recipes extends Activity{
	
	private HashSet<Consumption> ingredients;
	private ArrayList<Consumption> meals = new ArrayList<Consumption>();
	private RecipeAdapter rAdapter;
	private DatabaseHandler db = new DatabaseHandler(this);
	private ListView results;
	private EditText name;
	private EditText desc;
	AlertDialog.Builder alertBuilder;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Intent intent = getIntent();
		alertBuilder = new AlertDialog.Builder(this);
		String currentMealType = intent.getExtras().getString("meal_type");
		
		meals = db.getAllConsumptionItems(0, currentMealType);
		setContentView(R.layout.add_recipe);
		results = (ListView) findViewById(R.id.addRecipeList);
		ingredients = new HashSet<Consumption>();
		for (Consumption item : meals){
			ingredients.add(item); // start ingredients as all items in the current meal
		}
		rAdapter = new RecipeAdapter(this, meals, ingredients);
		results.setAdapter(rAdapter);
		
		name = (EditText) findViewById(R.id.addRecipeName);
		desc = (EditText) findViewById(R.id.addRecipeDescription);
		
		Button addIngredient = (Button) findViewById(R.id.addRecipeIngredient);
		addIngredient.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0) {
				Intent ingredientIntent = new Intent(Recipes.this, SearchList.class);
				Bundle b = new Bundle();
				b.putSerializable("mode", QuantitySelector.Mode.SELECT);
				b.putString("meal_type", "all");
				ingredientIntent.putExtras(b);
				startActivityForResult(ingredientIntent, RequestCode.SEARCH_LIST);
			}
		});
		
		Button complete = (Button) findViewById(R.id.addRecipeComplete);
		complete.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0){
				String recipeName = name.getText().toString();
				String recipeDescription = desc.getText().toString();
				
				if (recipeName.trim().equals("")){
					alertBuilder.setMessage("Recipe must have a name!");
					AlertDialog alert = alertBuilder.create();
					timerDelayRemoveDialog(3000, alert); //3 second timeout
					alert.show();
				}
				else if (ingredients.size() == 0){
					alertBuilder.setMessage("Recipe must have at least one ingredient!");
					AlertDialog alert = alertBuilder.create();
					timerDelayRemoveDialog(3000, alert);
					alert.show();
				}
				else{
					ArrayList<Consumption> ingredientList = new ArrayList<Consumption>();
					for (Consumption ingredient : ingredients){
						ingredientList.add(ingredient);
					}
					db.addRecipe(ingredientList, recipeName, recipeDescription);
					exit();
					Toast.makeText(getApplicationContext(), "Saved recipe for \"" + recipeName + "\"", Toast.LENGTH_LONG).show();
				}
			}
		});
		
		Button cancel = (Button) findViewById(R.id.addRecipeCancel);
		cancel.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0){
				exit();
			}
		});
	}
	
	public void timerDelayRemoveDialog(long time, final Dialog dialog){
		new Handler().postDelayed(new Runnable(){
			public void run(){
				dialog.dismiss();
			}
		}, time);
	}
	
	/**
	 * Catches the result of the QuantitySelection and edits the related consumption item
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		System.out.println("Caught activity result");
		if (requestCode == RequestCode.QUANTITY_SELECTION){
			System.out.println("Recognised quantity selection");
			if (resultCode == RESULT_OK){
				System.out.println("Result is OK");
				for (Consumption c : meals){
					if (c.getSurveyID() == data.getIntExtra("id", 0)){
						System.out.println("Found the corresponding item!");
						c.setQuantity(data.getIntExtra("quantity", 0));
						break;
					}
				}
				rAdapter.notifyDataSetChanged();
			}
		}
		else if (requestCode == RequestCode.SEARCH_LIST){
			if (resultCode == RESULT_OK){
				//ADD IT TO OUR LIST
				Consumption newItem = new Consumption(0, data.getIntExtra("id", 0), data.getIntExtra("quantity", 0), data.getStringExtra("short_name"));
				meals.add(newItem);
				ingredients.add(newItem);
				rAdapter.notifyDataSetChanged();
			}
		}
	}
	
	private void exit(){
		Intent returnIntent = new Intent();
		setResult(RESULT_OK, returnIntent);
		finish();
	}
}
