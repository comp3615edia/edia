package usyd.comp3615.edia;

import java.util.Calendar;
import java.util.Random;

import usyd.comp3615.edia.db.DatabaseHandler;
import usyd.comp3615.edia.db.ItemTree;
import usyd.comp3615.edia.server.LogService;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
 
public class Splash extends Activity {
 
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 1000;
    private DatabaseHandler db;
    public static int day = 0;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        
        setContentView(R.layout.activity_splash);
        db = new DatabaseHandler(this);
        //db.createDataBase();
        System.out.println("Tables: " + db.getTables());
        new Handler().postDelayed(new Runnable() {
 
            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */
 
            @Override
            public void run() {
                Random rand = new Random();
                
                // Initialise Service
                Intent i = new Intent(Splash.this, LogService.class);
                System.out.println("Latest commit: " + db.getLatestCommit());
                startService(i);
                PendingIntent pintent = PendingIntent.getService(Splash.this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarm = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
                
                // Set interval to run service between 2am to 3am.
                Calendar firingCal = Calendar.getInstance();
                Calendar currentCal = Calendar.getInstance();
                firingCal.set(Calendar.AM_PM, Calendar.AM);
                firingCal.set(Calendar.HOUR, 2);
                firingCal.set(Calendar.MINUTE, rand.nextInt(59));
                firingCal.set(Calendar.SECOND, rand.nextInt(59));
                
                
                // Get difference between current time and scheduled service time.
                long intendedTime = firingCal.getTimeInMillis();
                long currentTime = currentCal.getTimeInMillis();

                System.out.println("Minutes until sending data: " + (intendedTime - currentTime)/1000/60);
                
                // If service is scheduled to run, then execute. Otherwise, wait until next day
                if (intendedTime >= currentTime) {
                	alarm.setRepeating(AlarmManager.RTC, intendedTime, AlarmManager.INTERVAL_HALF_DAY, pintent);
                	
                }
                else {
                	firingCal.add(Calendar.DAY_OF_MONTH, 1);
                	intendedTime = firingCal.getTimeInMillis();
                	alarm.setRepeating(AlarmManager.RTC, intendedTime, AlarmManager.INTERVAL_HALF_DAY, pintent);
                }

                System.out.println("Minutes until sending data: " + (intendedTime - currentTime)/1000/60);
                
                // Check if user is logged in
                // CHANGE TO "!"
                if (db.isLoggedIn()) {
                	i = new Intent(Splash.this, Login.class);
                }
                else {
                	i = new Intent(Splash.this, MainActivity.class);
                }
                
                // loads the contents of the db into the item tree
        		ItemTree.getInstance().init(db); 
                
        		// loads the next screen
                startActivity(i);
                
                // closes this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
    
 
}