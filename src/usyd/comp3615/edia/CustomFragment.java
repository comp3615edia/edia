package usyd.comp3615.edia;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

public class CustomFragment extends Fragment {
	
	public static Fragment newInstance(MainActivity context, int pos, 
			float scale)
	{
		Bundle b = new Bundle();
		b.putInt("pos", pos);
		b.putFloat("scale", scale);
		return Fragment.instantiate(context, CustomFragment.class.getName(), b);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}
		
		LinearLayout l = (LinearLayout) 
				inflater.inflate(R.layout.button_switcher, container, false);
		
		int pos = this.getArguments().getInt("pos");
		
		Button btn = (Button) l.findViewById(R.id.content);
		
		switch(pos){
		case(0):
			btn.setBackgroundResource(R.drawable.breakfastbutton);
			OnClickListener listener = new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent(getActivity(), MealActivity.class);
					MealActivity.currentMealType = MealActivity.BREAKFAST;
					startActivity(intent);
				}
			};
			btn.setOnClickListener(listener);
			break;
		case(1):
			btn.setBackgroundResource(R.drawable.lunchbutton);
			OnClickListener listener2 = new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), MealActivity.class);
				MealActivity.currentMealType = MealActivity.LUNCH;
				startActivity(intent);
			}
		};
		btn.setOnClickListener(listener2);
			break;
		case(2):
			btn.setBackgroundResource(R.drawable.dinnerbutton);
			OnClickListener listener3 = new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), MealActivity.class);
				MealActivity.currentMealType = MealActivity.DINNER;
				startActivity(intent);
			}
		};
		btn.setOnClickListener(listener3);
			break;
		}	
		
		CustomLinearLayout root = (CustomLinearLayout) l.findViewById(R.id.root);
		float scale = this.getArguments().getFloat("scale");
		root.setScaleBoth(scale);

		
		return l;
	}
}