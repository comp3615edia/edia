package usyd.comp3615.edia.db;

public class Login {
	
	private int id;
	private String email;
	
	public Login() {
		
	}
	
	public Login(int id, String email) {
		this.setID(id);
		this.setEmail(email);
	}
	
	public Login(String email) {
		this.setID(1);
		this.setEmail(email);
	}

	public int getID() {
		return id;
	}

	public void setID(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}

