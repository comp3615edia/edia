package usyd.comp3615.edia.db;

import java.util.ArrayList;


public class Recipe {
	
	private ArrayList<Consumption> items;
	private String recipeName;
	private String recipeDescription;

	public Recipe(ArrayList<Consumption> items, String recipeName, String recipeDescription){
		this.items = items;
		this.recipeName = recipeName;
		this.recipeDescription = recipeDescription;
	}
	
	public Recipe(){
		
	}
	
	public void clear(){
		this.items = null;
		this.recipeName = null;
		this.recipeDescription = null;
	}

	public ArrayList<Consumption> getItems() {
		return items;
	}

	public void setItems(ArrayList<Consumption> items) {
		this.items = items;
	}

	public String getRecipeName() {
		return recipeName;
	}

	public void setRecipeName(String recipeName) {
		this.recipeName = recipeName;
	}

	public String getRecipeDescription() {
		return recipeDescription;
	}

	public void setRecipeDescription(String recipeDescription) {
		this.recipeDescription = recipeDescription;
	}
}
