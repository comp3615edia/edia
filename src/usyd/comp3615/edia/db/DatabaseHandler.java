package usyd.comp3615.edia.db;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import usyd.comp3615.edia.MealActivity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * DatabaseHandler deals with the all the connections and queries of the AUSNUT database
 * @author COMP3615 Group Q edia (tomislav, michael, runi, callum, sam)
 *
 */
public class DatabaseHandler extends SQLiteOpenHelper {

	// All Static variables
	private static final String TABLE_MEASURES = "measures";

	// Database Version
	private static final int DATABASE_VERSION = 8; // SQLite 3.7.9 2011-11-01 00:52:41
	// Database Path
	public static final String DB_PATH = "/data/data/usyd.comp3615.edia/databases/";
	// Database Name
	public static final String DB_NAME = "AUSNUTDatabase";

	// Database
	private SQLiteDatabase myDB;
	private final Context myContext;


	public static String USER = "";

	// Items table name
	private static final String TABLE_ITEMS = "items";

	private static final String TABLE_BRANDS = "brands";

	private static final String TABLE_CONSUMPTION = "consumption";

	private static final String TABLE_RECIPES = "recipes";

	//Favourites table name
	private static final String TABLE_FAVOURITES = "favourites";
	//Login table name
	private static final String TABLE_LOGIN = "login";

	// Items Table Columns names
	private static final String ITEM_ID = "_id";
	private static final String ITEM_NAME = "name";
	private static final String ITEM_DESCRIPTION = "description";
	private static final String BRAND_NAME = "brand";
	
	private static final String FAVOURITE_ITEM_ID = "survey_id";

	//Favourites Table Column names
	private static final String FAVOURITE_SHORTNAME = "short_name";
	//private static final String FAVOURITE_DATE = "last_used";

	//Consumption Table Column names
	private static final String CONSUMPTION_SURVEY = "survey_id";
	private static final String CONSUMPTION_NAME = "short_name";
	private static final String CONSUMPTION_QUANTITY = "quantity";
	private static final String CONSUMPTION_DATE = "time_eaten";
	private static final String CONSUMPTION_MEAL_TYPE = "meal_type";
	
	private static final String CONSUMPTION_LOCATION = "location";
	private static final String CONSUMPTION_TYPE = "meal_type";

	//Recipe Table Column names
	private static final String RECIPE_NAME = "recipe_name";
	private static final String RECIPE_DESCRIPTION = "recipe_description";

	//Login Table Column names
	//private static final String LOGIN_ID = "_id";
	private static final String LOGIN_EMAIL = "email";



	public DatabaseHandler(Context context) {
		super(context, DB_NAME, null, DATABASE_VERSION);
		this.myContext = context;
	}

	/**
	 * Creates a empty database on the system and rewrites it with our own database if one exists
	 * in the assets folder
	 * */
	public boolean createDataBase() {
		boolean dbExist = checkDataBase();
		if(!dbExist) {
			if (this.getReadableDatabase() == null) return false;
			
			try {
				copyDataBase();
			} catch (IOException e) {
				System.out.println("This ran 4");
				dbExist = false;
				System.out.println("Error copying database");
			}
		}
		return dbExist;
	}

	/**
	 * Checks to see if the database exists at the specified path
	 * @return True if it exists, False if not
	 */
	private boolean checkDataBase(){
		SQLiteDatabase checkDB = null;
		try{
			String myPath = DB_PATH + DB_NAME;
			// Line crashes
			checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
		}catch(SQLiteException e) {
			System.out.println("This ran 1");
			System.out.println("Database doesn't exist yet. Setting one up from assets");
		}catch(Exception e) {
			System.out.println("This ran 2");
			//Database doesn't exist
		}

		if(checkDB != null){
			System.out.println("This ran 3");
			checkDB.close();
		}
		return checkDB != null ? true : false;
	}
	
	public int getTables() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
		return c.getCount();
	}

	/**
	 * Copies the database from our local assets-folder to the just created empty database in the
	 * system folder, from where it can be accessed and handled.
	 * This is done by a transferring byte stream.
	 * */
	private void copyDataBase() throws IOException{
		// Opens a local database as the input stream
		InputStream myInput = myContext.getAssets().open(DB_NAME);
		System.out.println(myInput.read(new byte[1024]));
		

		// Path to the just created empty database
		String outFileName = DB_PATH + DB_NAME;

		// Opens the empty database as the output stream
		OutputStream myOutput = new FileOutputStream(outFileName);
		// Transfers bytes from the input file to the output file
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer))>0){
			myOutput.write(buffer, 0, length);
		}
		
		// Closes the streams
		myOutput.flush();
		myOutput.close();
		myInput.close();

	}

	/**
	 * Closes the SQL database stream from the object and the associated database file
	 */
	@Override
	public synchronized void close() {
		if(myDB != null)
			myDB.close();
		super.close();
	}


	@Override
	public void onCreate(SQLiteDatabase db) {
		
		System.out.println("THIS GETS EXECUTED");
		
		String CREATE_ITEMS = 
		"create table if not exists items(_id VARCHAR(8) PRIMARY KEY	NOT NULL, name STRING, description STRING);";
		String CREATE_MEASURES = 
		"create table if not exists measures(_id	VARCHAR(4) PRIMARY KEY	NOT NULL, survey_id	VARCHAR(8)	NOT NULL, quantity	INTEGER, measurement	VARCHAR(5), additional_description	STRING, volume	INTEGER, gram_weight	FLOAT, FOREIGN KEY(survey_id) REFERENCES items(_id));";
		String CREATE_BRANDS = 
		"create table if not exists brands(	_id	VARCHAR(4) PRIMARY KEY	NOT NULL,survey_id	VARCHAR(8)	NOT NULL,brand STRING,FOREIGN KEY(survey_id) REFERENCES items(_id));";
		String CREATE_FAVOURITES = 
		"create table if not exists favourites(_id VARCHAR(4) PRIMARY KEY	NOT NULL,survey_id VARCHAR(8) NOT NULL,name STRING,description STRING,short_name STRING,last_used INTEGER);";
		String CREATE_LOGIN = 
		"create table if not exists login(_id VARCHAR(1) PRIMARY KEY NOT NULL,email VARCHAR(30) NOT NULL);";
		String CREATE_CONSUMPTION = 
		"create table if not exists consumption(	_id	VARCHAR(2) PRIMARY KEY	NOT NULL,	survey_id VARCHAR(8) NOT NULL,	short_name STRING,	quantity	FLOAT	NOT NULL,	time_eaten	DATE	NOT NULL,	location STRING,	meal_type STRING);";
		String CREATE_RECIPES = 
		"create table recipes(_id VARCHAR(4) NOT NULL, survey_id VARCHAR(8) NOT NULL, quantity INTEGER NOT NULL, recipe_name STRING, recipe_description STRING)";
		String CREATE_COMMIT_LOG = 
		"create table commit_log(_id VARCHAR(4) NOT NULL, date STRING)";
		db.execSQL(CREATE_ITEMS);
		db.execSQL(CREATE_MEASURES);
		db.execSQL(CREATE_BRANDS);
		db.execSQL(CREATE_FAVOURITES);
		db.execSQL(CREATE_LOGIN);
		db.execSQL(CREATE_CONSUMPTION);
		db.execSQL(CREATE_RECIPES);
		db.execSQL(CREATE_COMMIT_LOG);
		try {
			insertItemsData(db);
			insertMeasuresData(db);
		} catch (IOException e) {
			System.out.println("error inserting data");
		}
		// Opens a local database as the input stream
	}
	
	private void insertItemsData(SQLiteDatabase db) throws IOException {
		InputStream myInput = myContext.getAssets().open("Items.csv");
		BufferedReader br = new BufferedReader(new InputStreamReader(myInput));

		// Transfers bytes from the input file to the output file
		String s = "";
		while ((s = br.readLine()) != null){
			String line[] = s.split("\t");
			ContentValues values = new ContentValues();
			values.put("_id", line[0]);
			values.put("name", line[1]);
			if (line.length > 2)
				values.put("description", line[2]);
			try {
				db.beginTransaction();
				db.insert("items", null, values);
				db.setTransactionSuccessful();
			}
			finally {
				db.endTransaction();
			}
		}
		
		myInput.close();
	}
	
	private void insertMeasuresData(SQLiteDatabase db) throws IOException {
		InputStream myInput = myContext.getAssets().open("Measures.csv");
		BufferedReader br = new BufferedReader(new InputStreamReader(myInput));
		//_id	VARCHAR(4) PRIMARY KEY	NOT NULL, 
		//survey_id	VARCHAR(8)	NOT NULL, 
		//quantity	INTEGER, 
		//measurement	VARCHAR(5), 
		//additional_description	STRING, 
		//volume	INTEGER, 
		//gram_weight	FLOAT
		// Transfers bytes from the input file to the output file
		String s = "";
		while ((s = br.readLine()) != null){
			String line[] = s.split("\t");
			ContentValues values = new ContentValues();
			values.put("_id", line[0]);
			values.put("survey_id", line[1]);
			values.put("quantity", line[2]);
			values.put("measurement", line[3]);
			values.put("additional_description", line[4]);
			values.put("volume", line[5]);
			values.put("gram_weight", line[6]);
			try {
				db.beginTransaction();
				db.insert(TABLE_MEASURES, null, values);
				db.setTransactionSuccessful();
			}
			finally {
				db.endTransaction();
			}
		}
		
		myInput.close();
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, -1);
		Date d = c.getTime();
		System.out.println(d.toString());
	}

	public boolean isLoggedIn() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_LOGIN, null);
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				USER = cursor.getString(1);
				return true;
			}
			else {
				return false;
			}
		}
		return false;
	}

	public void login(String email) {
		SQLiteDatabase db = this.getWritableDatabase();
		Login user = new Login(email);
		ContentValues values = new ContentValues();
		values.put("_id", user.getID());
		values.put(LOGIN_EMAIL, user.getEmail());
		try {
			db.beginTransaction();
			db.insert(TABLE_LOGIN, null, values);
			db.setTransactionSuccessful();
		}
		finally {
			db.endTransaction();
		}
		db.close(); // Closing database connection
	}

	public void logout() {
		SQLiteDatabase db = this.getWritableDatabase();
		String[] names = new String[1];
		names[0] = "1";
		db.delete(TABLE_LOGIN, "_id = ?",names);
	}

	/**
	 * Adds a new Item row to the items table
	 * @param item row to add
	 */
	public void addItem(Item item) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(ITEM_NAME, item.getName());
		values.put(ITEM_DESCRIPTION, item.getDescription());

		// Inserting Row
		db.insert(TABLE_ITEMS, null, values);
		db.close(); // Closing database connection
	}

	/**
	 * Gets an Item row from the items table
	 * @param id which specifies which row to get
	 * @return the Item object at id
	 */
	public Item getItem(int id) {
		SQLiteDatabase db = this.getWritableDatabase();

		Cursor cursor = db.query(TABLE_ITEMS, new String[] { ITEM_ID,
				ITEM_NAME, ITEM_DESCRIPTION }, ITEM_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();

		Item item = new Item(Integer.parseInt(cursor.getString(0)),
				cursor.getString(1), cursor.getString(2));
		cursor.close();
		db.close();
		// return contact
		return item;
	}

	public FavouriteItem getFavouriteItem(int id){
		FavouriteItem item = null;
		SQLiteDatabase db = this.getWritableDatabase();
		String selectQuery = "SELECT  * FROM " + TABLE_FAVOURITES + " WHERE survey_id = " + id;
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor != null){
			if (cursor.moveToFirst()){
				item = new FavouriteItem(Integer.parseInt(cursor.getString(1)),
				cursor.getString(2), cursor.getString(3), cursor.getString(4));
			}
			cursor.close();
		}
		db.close();
		// return contact
		return item;
	}

	public List<String> getBrands(int itemId){
		List<String> results = new ArrayList<String>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(TABLE_BRANDS, new String[] {BRAND_NAME}, ITEM_ID + "=?", new String[] { String.valueOf(itemId) }, null, null, null, null);
		if (cursor.moveToFirst()){
			do{
				results.add(cursor.getString(0));
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return results;
	}

	/**
	 * Gets all the names in the items table specified by the parameter given for the query
	 * @param str is the string for the query
	 * @return a list of all names in the items table beginning with the string associated with str
	 */
	public List<SearchResult> getNamesCal(String str) {
		List<SearchResult> searchResults = new ArrayList<SearchResult>();

		if (str.equals("")){
			return searchResults;
		}

		//get all the items in the database that have all the queries in their name
		String selectQuery;
		String[] input = str.split(" ");
		selectQuery = "SELECT LOWER(" + ITEM_NAME + ") FROM " + TABLE_ITEMS
				+ " WHERE " + ITEM_NAME + " LIKE \"%" + input[0] +"%\"";
		for (int i = 1; i != input.length; ++i){
			selectQuery += " AND " + ITEM_NAME + " LIKE \"%" + input[i] + "%\"";
		}

		selectQuery += " ORDER BY " + ITEM_NAME + " ASC"; //want to sort by index of input value

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		Set<String> results = new HashSet<String>(); // used to keep the results unique eg. apple, peeled, red skin and apple, peeled, green skin can both be shortened to apple, but we only want apple to appear once
		if (cursor.moveToFirst()){
			do{
				List<String> tiers = CSVParser.parse(cursor.getString(0)); //splits on commas, ignoring brackets
				Set<Integer> relevantTiers = new HashSet<Integer>();
				int maxTierAllTerms = 0;
				for (String term : input){
					int maxTierThisTerm = 0;
					for (int i = 0; i != tiers.size(); i++){
						if (tiers.get(i).contains(term)){
							maxTierThisTerm = (i > maxTierThisTerm? i : maxTierThisTerm);
							maxTierAllTerms = (maxTierThisTerm > maxTierAllTerms? maxTierThisTerm : maxTierAllTerms);
						}
					}
					relevantTiers.add(maxTierThisTerm);
				}

				String result = tiers.get(0);
				for (int i=1; i <= maxTierAllTerms; ++i){ //add all the required tiers
					result += "," + tiers.get(i);
				}
				if (!results.contains(result)){ //don't add to search results if there is already a search result named result
					results.add(result);
					searchResults.add(new SearchResult(result, relevantTiers));
				}
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();

		return searchResults;
	}

	/**
	 * Gets all the item rows in the items table
	 * @return a list of all items in the items table
	 */
	public List<Item> getAllItems() {
		List<Item> itemList = new ArrayList<Item>();
		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_ITEMS; 
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Item item = new Item();
				item.setID(Integer.parseInt(cursor.getString(0)));
				item.setName(cursor.getString(1));
				item.setDescription(cursor.getString(2));
				itemList.add(item);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		// return contact list
		return itemList;
	}

	/**
	 * Gets the number of rows in the items table
	 * @return the total rows
	 */
	public int getItemsCount() {
		String countQuery = "SELECT * FROM " + TABLE_ITEMS;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int val = cursor.getCount();
		cursor.close();
		db.close();
		return val;		
	}

	/**
	 * Updates an Item in the items table
	 * @param item is the row to update
	 * @return the id of where the item was updated
	 */
	public int updateItem(Item item) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(ITEM_NAME,  item.getName());
		values.put(ITEM_DESCRIPTION, item.getDescription());
		return db.update(TABLE_ITEMS,  values,  ITEM_ID + " = ?",
				new String[] { String.valueOf(item.getID()) });
	}

	/**
	 * Deletes an item row in the items table
	 * @param item to be removed
	 */
	public void deleteItem(Item item) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_ITEMS, ITEM_ID + " = ?",
				new String[] { String.valueOf(item.getID()) });
		db.close();
	}
	/**
	 * Gets all of the favourites from the database to fill the favourites activity
	 * @return the list of FavouriteItem's from the database.
	 */
	public ArrayList<FavouriteItem> getAllFavourites() {
		ArrayList<FavouriteItem> itemList = new ArrayList<FavouriteItem>();
		// Select All Query
		String selectQuery = "SELECT  * FROM favourites";

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor!=null&&cursor.moveToFirst()) {
			do {
				FavouriteItem item = new FavouriteItem();
				item.setID(Integer.parseInt(cursor.getString(1)));
				item.setName(cursor.getString(2));
				item.setDescription(cursor.getString(3));
				item.setFavouriteShortName(cursor.getString(4));
				// Adding item to list
				itemList.add(item);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		// return contact list
		return itemList;
	}
	/**
	 * addFavourite inserts a Favourite Item selected by a user into the favourites table.
	 * @param item - The item to be saved as a favourite.
	 * @param fDate - The last eaten date taken from Today().
	 */
	public void addFavourite(Item item, String name) {
		String selectQuery = "SELECT  * FROM " + TABLE_FAVOURITES;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		int id = cursor.getCount();
		Boolean add = true;
		for(int i = 0; i< id; ++i){
			if(item.getID() == Integer.parseInt(cursor.getString(1))){
				add = false;
			}
		}
		if(add){
			FavouriteItem fitem = new FavouriteItem(item,name);
			ContentValues values = new ContentValues();
			values.put("_id", id+1);
			values.put(FAVOURITE_ITEM_ID, fitem.getID());
			values.put(ITEM_NAME,  fitem.getName());
			values.put(ITEM_DESCRIPTION, fitem.getDescription());
			values.put(FAVOURITE_SHORTNAME, fitem.getFavouriteShortName());
			try {
				db.beginTransaction();
				db.insert(TABLE_FAVOURITES, null, values);
				db.setTransactionSuccessful();
			}
			finally {
				db.endTransaction();
			}
		}
		cursor.close();
		db.close();
	}

	public void addFavourite(FavouriteItem fitem){
		String selectQuery = "SELECT  * FROM " + TABLE_FAVOURITES;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		int id = cursor.getCount();
		Boolean add = true;
		
		if (cursor!=null&&cursor.moveToFirst()) {
			do {
				if(fitem.getID() == Integer.parseInt(cursor.getString(0))){
					add = false;
				}
			} while (cursor.moveToNext());
		}
		
		if(add){
			ContentValues values = new ContentValues();
			values.put("_id", id+1);
			values.put(FAVOURITE_ITEM_ID, fitem.getID());
			values.put(ITEM_NAME,  fitem.getName());
			values.put(ITEM_DESCRIPTION, fitem.getDescription());
			values.put(FAVOURITE_SHORTNAME, fitem.getFavouriteShortName());
			try {
				db.beginTransaction();
				db.insert(TABLE_FAVOURITES, null, values);
				db.setTransactionSuccessful();
			}
			finally {
				db.endTransaction();
			}
		}
		cursor.close();
		db.close();
	}

	/**
	 * deleteFavourite deletes a Favourite Item selected by a user from the favourites table.
	 * @param item - The item to be removed from favourites.
	 */
	public void deleteFavourite(Item item) {
		SQLiteDatabase db = this.getWritableDatabase();
		String[] names = new String[1];
		names[0] = String.valueOf(item.getID());
		db.delete(TABLE_FAVOURITES, "survey_id = ?",names);
		db.close();
	}

	/**
	 * Adds the item and the given quantity and timestamp of now to the device database
	 * @param item: The item to be added
	 * @param quantity: The gram_weight quantity that was consumed
	 * @param timestamp: The time the item was consumed
	 */
	public boolean addConsumption(int itemID, String name, float quantity, String location, String mealType){
		mealType = sanitateMealType(mealType);
		
		String selectQuery = "SELECT _id FROM " + TABLE_CONSUMPTION;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		cursor.moveToFirst();
		int id = 0;
		if (cursor!=null&&cursor.moveToFirst()) {
			do {
				 int i = cursor.getInt(0);
				 if (id < i) id = i;
			} while (cursor.moveToNext());
		}
		

		ContentValues values = new ContentValues();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		
		values.put("_id", id+1);
		values.put(CONSUMPTION_SURVEY, itemID);
		values.put(CONSUMPTION_NAME, name);
		values.put(CONSUMPTION_QUANTITY, quantity);
		values.put(CONSUMPTION_DATE, dateFormat.format(new Date()));
		values.put(CONSUMPTION_LOCATION, location);
		values.put(CONSUMPTION_TYPE, mealType);
		try {
			db.beginTransaction();
			db.insert(TABLE_CONSUMPTION, null, values);
			db.setTransactionSuccessful();
		}
		finally {
			db.endTransaction();
		}
		cursor.close();
		db.close();
		return true;
	}

	/**
	 * Checks whether a particular item has been consumed in a particular meal in today, and returns the consumption if it has.
	 * @return The consumption if it exists, and null otherwise
	 */
	public Consumption checkForConsumption(int itemID, String mealType){
		mealType = sanitateMealType(mealType);
		Consumption item = null;
		String selectQuery = "SELECT * FROM consumption"
				+	" WHERE " + CONSUMPTION_DATE + " LIKE date('now', '-0 day', 'localtime') || '%'"
				+	" AND " + CONSUMPTION_SURVEY + " = ?"
				+	" AND " + CONSUMPTION_MEAL_TYPE + "= ?";
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, new String[]{Integer.toString(itemID), mealType});
		if (cursor != null && cursor.moveToFirst()){ //there should only ever be one result, so we don't loop
			item = new Consumption(Integer.parseInt(cursor.getString(0)));
			item.setSurveyID(Integer.parseInt(cursor.getString(1)));
			item.setShortName(cursor.getString(2));
			item.setQuantity(Float.parseFloat(cursor.getString(3)));
			item.setTime_eaten(cursor.getString(4));
			item.setLocation(cursor.getString(5));
			item.setMealType(cursor.getString(6));
		}
		cursor.close();
		db.close();
		return item;
	}
	
	public boolean checkForRecipe(String name){
		boolean exists = false;
		String selectQuery = "SELECT * FROM " + TABLE_RECIPES
				+	" WHERE " + RECIPE_NAME + "= ?";
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, new String[]{name});
		if (cursor != null && cursor.moveToFirst()){ //there should only ever be one result, so we don't loop
			exists = true;
		}
		cursor.close();
		db.close();
		return exists;
	}

	/**
	 * Gets all of the favourites from the database to fill the favourites activity
	 * @param i, number of days prior to collect the consumptions item
	 * @return the list of FavouriteItem's from the database.
	 */
	public ArrayList<Consumption> getAllConsumptionItems(int i, String mealType) {
		mealType = sanitateMealType(mealType);
		i = Math.abs(i);
		String selectQuery;

		String[] args = null;
		if (mealType.equals("all"))
		{
			// This is for when this method is called in the LogService.java file. Since that file needs all the items, the String passed is null.
			selectQuery = "SELECT * FROM consumption WHERE " + CONSUMPTION_DATE + " LIKE date('now', '-"+i+" days', 'localtime') || '%'";
		} else {
			selectQuery = "SELECT * FROM consumption WHERE " + CONSUMPTION_DATE
					+ " LIKE date('now', '-"+i+" day', 'localtime') || '%' AND " + CONSUMPTION_TYPE + " LIKE '%" +  mealType + "%'";
		}

		System.out.println(selectQuery);
		
		ArrayList<Consumption> itemList = new ArrayList<Consumption>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, args);

		// looping through all rows and adding to list
		if (cursor!=null&&cursor.moveToFirst()) {
			do {
				Consumption item = new Consumption(Integer.parseInt(cursor.getString(0)));
				item.setSurveyID(Integer.parseInt(cursor.getString(1)));
				item.setShortName(cursor.getString(2));
				item.setQuantity(Float.parseFloat(cursor.getString(3)));
				item.setTime_eaten(cursor.getString(4));
				item.setLocation(cursor.getString(5));
				item.setMealType(cursor.getString(6));
				//System.out.println(item.toString());
				// Adding item to list
				itemList.add(item);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		// return contact list
		return itemList;
	}

	/**
	 * Removes the consumption entry from the consumption table
	 * @param c: The consumption object to be deleted.
	 */
	public void deleteConsumption(Consumption c){
		SQLiteDatabase db = this.getWritableDatabase();
		String[] names = new String[1];
		names[0] = String.valueOf(c.getId());
		try {
			db.beginTransaction();
			db.delete(TABLE_CONSUMPTION, "_id = ? ",names);
			db.setTransactionSuccessful();
		}
		finally {
			db.endTransaction();
		}
		db.close();
	}

	public void updateConsumption(int id, float quantity){
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();
		String[] ids = new String[1];
		ids[0] = String.valueOf(id);
		cv.put(CONSUMPTION_QUANTITY, quantity);
		try {
			db.beginTransaction();
			db.update(TABLE_CONSUMPTION, cv, "_id = ?", ids);
			db.setTransactionSuccessful();
		}
		finally {
			db.endTransaction();
		}
		db.close();
	}


	/**
	 * Gets the measurement options from the database that match the Item.
	 * @param item: The item to get the measurements of.
	 * @return HashMap<String,Double> of the measurement name and its gram weight.
	 */
	public HashMap<String, Double> getMeasures(Item item){
		HashMap<String, Double> measures = new HashMap<String,Double>();
		String selectQuery = "SELECT  measurement, gram_weight FROM " + TABLE_MEASURES + " WHERE survey_id = " + item.getID();
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor!=null&&cursor.moveToFirst()) {
			do {
				measures.put(cursor.getString(0), cursor.getDouble(1));
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return measures;
	}

	/**
	 * Returns the id of the first measurement that matches the Item
	 * @param item
	 * @return id of the first measurement
	 */
	public int getMeasureID(Item item){
		int measureID = 0;
		String selectQuery = "SELECT  _id FROM " + TABLE_MEASURES + " WHERE survey_id = " + item.getID();
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst())
		{
			measureID = Integer.parseInt(cursor.getString(1));
		}
		cursor.close();
		db.close();
		return measureID;
	}

	public void addRecipe(ArrayList<Consumption> items, String name, String description){
		String selectQuery = "SELECT  * FROM " + TABLE_RECIPES;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		int id = 0;
		if (cursor!=null&&cursor.moveToFirst()) {
			do {
				id = Integer.parseInt(cursor.getString(0));
			} while (cursor.moveToNext());
		}
		for(Consumption c : items){
			ContentValues cv = new ContentValues();
			cv.put(ITEM_ID, id);
			cv.put(CONSUMPTION_SURVEY, c.getSurveyID());
			cv.put(CONSUMPTION_QUANTITY, c.getQuantity());
			cv.put(RECIPE_NAME, name);
			cv.put(RECIPE_DESCRIPTION, description);
			try {
				db.beginTransaction();
				db.insert(TABLE_RECIPES, null, cv);
				System.out.println("successfully added recipe");
				db.setTransactionSuccessful();
			}
			finally {
				db.endTransaction();
			}
		}
		cursor.close();
		db.close();
	}

	public ArrayList<Recipe> getAllRecipes(){
		String selectQuery = "SELECT  * FROM " + TABLE_RECIPES;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		ArrayList<Recipe> recipes = new ArrayList<Recipe>();
		Recipe r = new Recipe();
		ArrayList<Consumption> c = new ArrayList<Consumption>();
		int id = 0;
		int check = 0;
		if (cursor!=null&&cursor.moveToFirst()) {
			do {
				//Check that the rows belong to the same recipe
				if(Integer.parseInt(cursor.getString(0))!=id){
					//Finish the recipe off
					r.setItems(c);
					//Add the recipe
					recipes.add(r);
					//Reset the objects for the next recipe
					r.clear();
					c.clear();
					check = 0;
					//Prepare for the next recipe
					id++;	
				}
				//Set the name and description once
				if(check == 0){
					r.setRecipeName(cursor.getString(3));
					r.setRecipeDescription(cursor.getString(4));
					check++;
				}
				//Get the consumption out of the row and add it to the list
				Consumption con = new Consumption(Integer.parseInt(cursor.getString(0)));
				con.setSurveyID(Integer.parseInt(cursor.getString(1)));
				con.setQuantity(Float.parseFloat(cursor.getString(2)));
				c.add(con);
			} while (cursor.moveToNext());
		}
		return recipes;
	}

	public Recipe getRecipe(int id){
		String selectQuery = "SELECT  * FROM " + TABLE_RECIPES + " WHERE _id = " + id;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		Recipe r = new Recipe();
		ArrayList<Consumption> c = new ArrayList<Consumption>();
		int check = 0;
		if (cursor!=null&&cursor.moveToFirst()) {
			do {
				//Set the recipe name and description once
				if(check ==0){
					r.setRecipeName(cursor.getString(3));
					r.setRecipeDescription(cursor.getString(4));
					check++;
				}
				//Get each consumption row.
				Consumption con = new Consumption(Integer.parseInt(cursor.getString(0)));
				con.setSurveyID(Integer.parseInt(cursor.getString(1)));
				con.setQuantity(Float.parseFloat(cursor.getString(2)));
				c.add(con);
				
			} while (cursor.moveToNext());
		}
		//Add the list to the recipe
		r.setItems(c);
		return r;
	}
	
	public String sanitateMealType(String mealType) {
		int i = 0;
		String types[] = {	MealActivity.BREAKFAST,
						  	MealActivity.LUNCH,
						  	MealActivity.DINNER,
						  	MealActivity.SNACK,
						  	"all"};
		for (String s : types) {
			if (!mealType.equals(s)) {
				i++;
			}
		}
		
		if (i > types.length - 1) 
			return "snack";
		else
			return mealType;
	}
	
	public boolean addCommit(String string) {
		String selectQuery = "SELECT  MAX(_id) FROM commit_log";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		cursor.moveToFirst();
		int id = cursor.getInt(0);
		
		ContentValues values = new ContentValues();
		
		values.put("_id", id+1);
		values.put("date", string);
		try {
			db.beginTransaction();
			db.insert("commit_log", null, values);
			db.setTransactionSuccessful();
		}
		finally {
			db.endTransaction();
		}
		cursor.close();
		db.close();
		return true;
	}
	
	public String getLatestCommit() {
		String selectQuery = "SELECT date FROM commit_log";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				System.out.println(cursor.getString(0));
			} while (cursor.moveToNext());
		}
		if (!cursor.moveToLast()) {
			return "";
		}
		String commit = cursor.getString(0);
		cursor.close();
		db.close();
		return commit;
	}

	
	public void deleteRecipe(Recipe r){
		SQLiteDatabase db = this.getWritableDatabase();
		String[] names = new String[1];
		names[0] = String.valueOf(r.getRecipeName());
		try {
			db.beginTransaction();
			db.delete(TABLE_RECIPES, "recipe_name = ? ",names);
			db.setTransactionSuccessful();
		}
		finally {
			db.endTransaction();
		}
		db.close();
	}

	public void setLatestCommit() {
		SQLiteDatabase db = this.getWritableDatabase();
		String[] a = {"2013/11/01"};
		try {
			db.beginTransaction();
			db.delete("commit_log", "date = ? ", a);
			db.setTransactionSuccessful();
		}
		finally {
			db.endTransaction();
		}
		db.close();
	}
}
