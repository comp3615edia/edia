package usyd.comp3615.edia.db;

import java.util.Set;

/**
 * Class for a result of a search - holds the search result string, which can be used as a path to a node in the ItemTree, and a set of integers which refer to which tiers contain the search query
 * @author CJR
 *
 */
public class SearchResult{
	Set<Integer> relevantTiers;
	String nodePath;
	
	public SearchResult(String nodePath, Set<Integer> relevantTiers){
		this.nodePath = nodePath;
		this.relevantTiers = relevantTiers;
	}
	
	public String getNodePath(){
		return nodePath;
	}
	
	public Set<Integer> getRelevantTiers(){
		return relevantTiers;
	}
	
}