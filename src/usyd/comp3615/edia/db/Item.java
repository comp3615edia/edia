package usyd.comp3615.edia.db;

public class Item {
	
	private int id;
	private String name;
	private String description;
	
	public Item() {
		
	}
	
	public Item(int id, String name, String description) {
		this.setID(id);
		this.setName(name);
		this.setDescription(description);
	}
	
	public Item(String name, String description) {
		this.setName(name);
		this.setDescription(description);
	}

	public int getID() {
		return id;
	}

	public void setID(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
}
