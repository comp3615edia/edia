package usyd.comp3615.edia.db;


public class FavouriteItem extends Item{

	private String favouriteShortName;

	public FavouriteItem() {
		super();
	}

	public FavouriteItem(int id, String name, String description, String shortName) {
		super(id, name, description);
		this.setFavouriteShortName(shortName);
	}

	public FavouriteItem(String name, String description, String shortName) {
		super(name,description);
		this.setFavouriteShortName(shortName);
	}

	public FavouriteItem(Item item, String name){
		super(item.getID(),item.getName(),item.getDescription());
		this.setFavouriteShortName(name);
	}

	public String getFavouriteShortName() {
		return favouriteShortName;
	}

	public void setFavouriteShortName(String shortFavouriteName) {
		this.favouriteShortName = shortFavouriteName;
	}
}
