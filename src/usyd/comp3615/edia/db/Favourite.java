package usyd.comp3615.edia.db;

public class Favourite {
	
	private int id;
	private String name;
	private String description;
	private String shortName;
	private String lastUsed;
	
	public Favourite() {
		
	}
	
	public Favourite(int id, String name, String description, String shortName, String lastUsed) {
		this.setID(id);
		this.setName(name);
		this.setDescription(description);
		this.setShortName(description);
		this.setLastUsed(description);
	}
	
	public Favourite(String name, String description) {
		this.setName(name);
		this.setDescription(description);
	}

	public int getID() {
		return id;
	}

	public void setID(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	
	public String getLastUsed() {
		return lastUsed;
	}

	public void setLastUsed(String lastUsed) {
		this.lastUsed = lastUsed;
	}

	
}
