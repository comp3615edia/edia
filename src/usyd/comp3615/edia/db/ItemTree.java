package usyd.comp3615.edia.db;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;



/**
 * Used to categorise items, creates a tree using the comma-delimited item names by grouping items with common aspects.
 * Provides a way of returning simplified search results.
 * @author Callum 
 */
public class ItemTree{

	private static ItemTree instance;
	@SuppressWarnings("unused")
	private List<Node> brandsList = new ArrayList<Node>();
	
	/**
	 * The node class used by the ItemTree object - every node keeps track of its parent, children, name, and Item (if it qualifies as an item and not just a category)
	 */
	public class Node{
		List<Node> children;
		Item item;
		String tierName;
		Node parent;
		int subtreeSize;
		
		/**
		 * Constructs a new Node object the a given name
		 * @param name The name that is given to the new node instance
		 */
		public Node(String name){
			item = null;
			tierName = name.trim().toLowerCase();
			children = new ArrayList<Node>();
		}
		
		/**
		 * Returns the name of the node
		 * @return The nodes name
		 */
		public String getName(){
			return tierName;
		}
		
		/**
		 * Returns the name of the category followed by its ancestors name (the highest super-category)
		 * @return The name associated with this node that would show in the item results (eg. peeled apple)
		 */
		public String getFullName(){
			Node ancestor = parent;
			while (ancestor.parent != null){
				ancestor = ancestor.parent;
			}
			return tierName + " " + ancestor.tierName;
		}
		
		/**
		 * Returns the parent of the node
		 * @return The nodes parent
		 */
		public Node getParent(){
			return parent;
		}
		
		/**
		 * Returns all the children associated with this node 
		 * @return A list of Nodes that are the children of this node
		 */
		public List<Node> getChildren(){
			return children;
		}
		
		/**
		 * Adds a given node as a child of this node, and sets the childs parent to this node. If this node is an item, adding a child will convert it to a category with an extra child named "not further specified" that corresponds to the item that was previously associated with this node.
		 * Children are stored in alphabetical order to allow the findNode() method to perform a binary search
		 * @param child The node that is to be made a child of this node
		 */
		public void addChild(Node child){	
			children.add(child);
			child.parent = this;
		}
		
		/**
		 * Returns the size of the subtree rooted at this node. This is equal to the number of items (leaf nodes) that belong to this category and its sub-categories 
		 * @return The size of the subtree rooted at this node (the number of items that are encapsulated by this category)
		 */
		public int getSubtreeSize(){
			//if the subtree size has already been set, just return it instead of recalculating
			if (subtreeSize != 0)
				return subtreeSize;
			
			//if the node has no children, it is a leaf and therefore the size of its subtree is just 1 (the node itself)
			if (item != null){
				subtreeSize = 1;
			}
			
			//if the node isn't a leaf, the size of the subtree is equal to the sum of its childrens subtrees 
			for (Node child : children){
				subtreeSize += child.getSubtreeSize();
			}
			return subtreeSize;
		}
		
		/**
		 * Sets the item associated with this node to the given argument
		 * @param item The item that should be associated with this node
		 */
		public void setItem(Item item){
			this.item = item;
		}
		
		/**
		 * Returns the item associated with this node
		 * @return The item associated with this node, or null if this node responds to a category instead
		 */
		public Item getItem(){
			return item;
		}
	}
	
	/**
	 * Used by the init method to sort the items in order to speed up construction and searches.
	 * This is a private class since the ItemTree is the only object that needs these items sorted in this way.
	 * @author Callum
	 */
	private class ItemNameComparator implements Comparator<Item>{
		public int compare(Item arg0, Item arg1) {
			//Since EOL < space < comma, the middle tiers end up ranked differently to the end ones since the middles end with commas and the ends end with EOL's
			//To fix this, we ensure that all tiers end with commas by ordering them as though there was a comma attached to the end tier
			String firstName = arg0.getName() + ",";
			String secondName = arg1.getName() + ",";
			return firstName.compareTo(secondName);
		}
	}
	
	Node root;
	
	/**
	 * Returns the ItemTree singleton instance, 
	 * @return The ItemTree singleton
	 */
	public static ItemTree getInstance(){
		if (instance == null){
			instance = new ItemTree();
		}
		return instance;
	}
	
	/**
	  * Creates a new item tree with a single node - a root with no name.
	  * The tree needs to be initialized using the init() method.
	  */
	public ItemTree(){
		root = new Node("");
		brandsList = new ArrayList<Node>();
	}
	
	/**
	 * Constructs a new ItemTree containing all the items in the database 
	 * @param dbHandler Uses a DatabaseHandler object to get all the items from the SQLite database for transformation
	 */
	public void init(DatabaseHandler dbHandler){
		root = new Node(""); //the root represents the whole Item class, it holds all the items together but has no value itself 
				
		Node deepestTierNode = root; //this variable keeps track of the last CategoryNode used, which may be re-used by the following item if they are similar items
		int deepestTierNum = -1; //this variable keeps track of the index in the tiers array that the above category was found, to allow us to quickly check if a following item is also in that category
		List<String> previousItemName = new ArrayList<String>();
		List<Item> sortedItems = dbHandler.getAllItems();
			
//		PLACEHOLDER - EDIT THE ACTUAL DATABASE!!!!
		for (Item item : sortedItems){
			item.setName(item.getName().toLowerCase().replaceAll("\\s+", " "));
		}
//		//END OF PLACEHOLDER
		
		Collections.sort(sortedItems, new ItemNameComparator());
		
		
		for (Item item : sortedItems){ //for every item in the database
			
			String itemName = item.getName();
			
			List<String> tiers = CSVParser.parse(itemName); //split the items name on commas using the CSVParser, which will ignore commas if they are nested in brackets
			
			//find what tier the first change occurs in
			int lastRelevantTier = -1;
			int i = 0;
			while (i < tiers.size() && i < previousItemName.size() && tiers.get(i).equals(previousItemName.get(i))){
					lastRelevantTier = i;
					i++;
			}
			previousItemName = tiers;
			
			//climb back up the tree until we reach the tier with the first change
			while (deepestTierNum > lastRelevantTier){
				deepestTierNode = deepestTierNode.getParent();
				deepestTierNum--;
			}
			
			//add all the tiers that don't already exist in the tree by branching off from the node found in the above section
			while (deepestTierNum < (tiers.size()-1)){
				deepestTierNum++;
				Node newNode = new Node(tiers.get(deepestTierNum));
				deepestTierNode.addChild(newNode);
				deepestTierNode = newNode;
			}
			//associate the last added node with the item
			deepestTierNode.setItem(item);
			
			//now add the brands associated with this item
//			for (String brand : dbHandler.getBrands(item.getID())){
//				List<String> brandTiers = CSVParser.parse(brand);
//				
//			}
		}
	}
	
	/**
	 * Finds all nodes in the ItemTree that contain the query in their name by performing a BFS from the root of the tree
	 * @param query The string that must be contained within the nodes name for it to qualify as a result
	 * @return A list of nodes that contain the query in their name
	 */
	public List<Node> getNodesContaining(String query){
		return BFS(query, root);
	}
	
	/**
	 * Finds all nodes in the ItemTree that contain all of the queries in their expanded name (the product of their name and all their ancestors names).
	 * @param queries An array of strings that must all be contained within the nodes expanded name.
	 * @return A list of nodes that contain all of the given strings in their expanded name.
	 */
	public List<Node> getNodesContaining(String[] queries){
		
		List<Node> currentNodeSet = BFS(queries[0], root); //finds all results containing the first query
		List<Node> newNodeSet = new ArrayList<Node>();
		
		for (int i=1; i != queries.length; ++i){
			for (Node node : currentNodeSet){
				newNodeSet.addAll(BFS(queries[i], node)); //searches only the subtrees containing the (i-1)th query for nodes containing the ith query
			}
			currentNodeSet = newNodeSet;
			newNodeSet = new ArrayList<Node>();
		}
		return currentNodeSet;
	}
	
	/**
	 * Performs a BFS on the subtree rooted at the given node for nodes containing a given query in their name
	 * @param query The string that must be contained within the nodes name for it to qualify as a result
	 * @param root The node at which the subtree is rooted
	 * @return A list of nodes that contain the query in their name
	 */
	private List<Node> BFS(String query, Node root){
		List<Node> nodeList = new ArrayList<Node>();
		LinkedList<Node> nodesToView = new LinkedList<Node>(); 
		
		nodesToView.push(root);
		while (!nodesToView.isEmpty()){
			Node currentNode = nodesToView.pop();
			if (currentNode.getName().contains(query)){
				nodeList.add(currentNode);
			}
			else{
				for (Node child : currentNode.getChildren()){
					nodesToView.push(child);
				}
			}
		}
		return nodeList;
	}
	
	/**
	 * This method finds the node associated with an item or category name
	 * @param name The name of the item or category to find (eg. "Apple, peeled")
	 * @return The node associated with the given name (eg. the child of Apple named peeled)
	 */
	public Node findNode(String name){
		name = name.replaceAll("\\s+", " "); //PLACEHOLDER - remove double spaces from db permanently!
		
		List<String> tiers = CSVParser.parse(name);
		Node currentNode = root;
		for (int i=0; i!=tiers.size(); ++i){	
			String currentTier = tiers.get(i) + ",";

			boolean found = false;
			int min = 0, max = currentNode.getChildren().size()-1; 
			while (min <= max){ //do a binary search for the node
				Node child = currentNode.getChildren().get(min + ((max-min) / 2));
				
				String childName = child.getName() + ",";
				
				if (childName.compareTo(currentTier) < 0){
					min = min + ((max-min) / 2)+1;
				}
				else if (childName.compareTo(currentTier) > 0){
					max = min + ((max-min) / 2)-1;
				}
				else{
					currentNode = child;
					found = true;
					break;
				}
			}
				
			if (!found){
				// We shouldn't return a null. This causes errors for us!!
				return null;
			}
		}
		return currentNode;
	}
	
	/**
	  * Prints out the entire item tree to System.out, using indenting to show relationships
	  */
	public void printTree(){
	
		for (Node child : root.getChildren()){
			printSubtree(child, 0);
		}
	}
	
	/**
	  * Supporting method for printTree(), used to provide a recursive approach
	  * @param root The root of the subtree which we are currently printing
	  * @param depth The depth of the root node in the item tree (how many ancestors the given node has) 
	  */
	private void printSubtree(Node root, int depth){
		for (int i=0; i!=depth; ++i){
			System.out.print("- ");
		}
		System.out.println(root.getName() + ";");
		for (Node child : root.getChildren()){
			printSubtree(child, depth+1);
		}
	}
	
	/**
	 * Returns the number of items contained within the tree
	 * @return The number of items in the tree
	 */
	public int getSize(){
		return root.getSubtreeSize();
	}
	
}