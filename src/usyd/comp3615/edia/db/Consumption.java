package usyd.comp3615.edia.db;

public class Consumption {
	private int id;
	private int survey_id;
	private float quantity;
	private String short_name;
	private String time_eaten;
	private String location;
	private String meal_type;
	
	public Consumption(int id) {
		this.id = id;
	}
	
	public Consumption(int id, int survey_id) {
		this.id = id;
		this.survey_id = survey_id;
	}
	
	public Consumption(int id, int survey_id, int quantity) {
		this.id = id;
		this.survey_id = survey_id;
		this.setQuantity(quantity);
	}
	
	public Consumption(int id, int survey_id, int quantity, String shortName) {
		this.id = id;
		this.survey_id = survey_id;
		this.setQuantity(quantity);
		this.short_name = shortName;
	}
	
	public Consumption(int id, int survey_id, int quantity, String shortName, String location) {
		this.id = id;
		this.survey_id = survey_id;
		this.setQuantity(quantity);
		this.short_name = shortName;
		this.location = location;
	}
	
	public Consumption(int id, int survey_id, int quantity, String shortName, String location, String meal_type) {
		this.id = id;
		this.survey_id = survey_id;
		this.setQuantity(quantity);
		this.short_name = shortName;
		this.location = location;
		this.meal_type = meal_type;
	}
	
	public Consumption(int survey_id, int quantity, String shortName, String location, String meal_type) {
		this.survey_id = survey_id;
		this.setQuantity(quantity);
		this.short_name = shortName;
		this.location = location;
		this.meal_type = meal_type;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the survey_id
	 */
	public int getSurveyID() {
		return survey_id;
	}
	/**
	 * @param survey_id the survey_id to set
	 */
	public void setSurveyID(int survey_id) {
		this.survey_id = survey_id;
	}

	/**
	 * @return the quantity
	 */
	public float getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}

	public String getShortName() {
		return short_name;
	}

	public void setShortName(String short_name) {
		this.short_name = short_name;
	}

	/**
	 * @return the time_eaten
	 */
	public String getTime_eaten() {
		return time_eaten;
	}

	/**
	 * @param time_eaten the time_eaten to set
	 */
	public void setTime_eaten(String time_eaten) {
		this.time_eaten = time_eaten;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}
	/**
	 * @return the meal_type
	 */
	public String getMealType() {
		return meal_type;
	}
	/**
	 * @param meal_type the meal_type to set
	 */
	public void setMealType(String meal_type) {
		this.meal_type = meal_type;
	}
	
	@Override
	public String toString() {
		return String.format("%s, %s, %s, %s, %s, %s, %s", getId(), getSurveyID(), getShortName(), 
				getQuantity(), getTime_eaten(), getLocation(), getMealType());
	}
}
