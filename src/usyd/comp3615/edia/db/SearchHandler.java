package usyd.comp3615.edia.db;

import java.util.ArrayList;
import java.util.List;


public class SearchHandler {
	
	private List<String> names = new ArrayList<String>();
	private List<String> ids = new ArrayList<String>();
	private List<Integer> positions = new ArrayList<Integer>();
	private int limit = 0;
	
	public SearchHandler() {
		
	}
	
	public void add(String name, String id, int position) {
		// If more than 20 items. Stop!
		if (limit > 20) return;
		
		// Remove all white space
		id = id.trim();
		
		// Remove any items that are ill-formated
		if (id.contains("(") || id.contains(")")) return;
		
		// Change comma separated item to be that of the full name up until the end of the next comma section.
		id = name.substring(0, name.indexOf(id) + id.length());
		
		/*
		// id = id + " " + name.substring(0, name.indexOf(id) - 2);
		*/
		/*
		//
		int loc = name.indexOf(id);
		// Change comma separated item to be that of the full name up until the end of the next comma section.
		if (name.contains(",")) {
			// NEEDS FIXING
			id = name.substring(loc, loc + id.length()) + name.substring(0, loc);
		}
		//
		 * */
		 
		
		// Ensure that no names with the first 10 characters are repeated (good for minimising search results)
		name = name.substring(0, Math.min(10, name.length()));
		
		// Return if the primary name already exists
		if (names.contains(name)) return;
		
		// Return if the identifier already exists
		if (ids.contains(id)) return;
		
		// Add item to list
		names.add(name);
		ids.add(id);
		positions.add(position);
		
		// Add one to item added
		limit++;
		
	}
	
	public List<String> getNames() {
		return names;
	}
	
	
	public List<String> getIDs() {
		return ids;
	}

}
