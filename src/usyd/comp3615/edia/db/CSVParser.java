package usyd.comp3615.edia.db;
import java.util.ArrayList;
import java.util.List;

public class CSVParser{
	public static List<String> parse(String arg){	
		List<String> results = new ArrayList<String>();
		boolean ignoreCommas = false;
		int startIndex = 0;
		for (int i=0; i!=arg.length(); ++i){ //for each character in the given string
			char currentChar = arg.charAt(i);			
			if (ignoreCommas){ //if we are in the ignore commas state, we don't react to any character except a closed bracket, which switches us back to the normal state
				if (currentChar == ')'){
					ignoreCommas = false;
				}
			}
			else{ //if we are not ignoring commas
				if (currentChar == '('){ //if the current character is an open bracket, we have to start ignoring commas
					ignoreCommas = true;
				}
				else if (currentChar == ','){ //if the current character is a comma, we can create a new substring that starts where the last comma was found and ends at this point
					String result = sanitize(arg.substring(startIndex, i).trim().toLowerCase());
					results.add(result);
					startIndex = i+1;
				}
			}
		}
		String result = sanitize(arg.substring(startIndex, arg.length()).trim().toLowerCase());
		results.add(result); //when we reach the end of the word, we also add from the last comma found to the end of the word as a substring
		return results;
	}
	
	private static String sanitize(String arg){
		if (arg.startsWith("and ")){
			arg = arg.replaceFirst("and ", "");
		}
		else if (arg.startsWith("or")){
			arg = arg.replaceFirst("or ", "");
		}
		else if (arg.startsWith("& ")){ 
			arg = arg.replaceFirst("& ", "");
		}
		return arg;
	}
}