package usyd.comp3615.edia;

import java.util.ArrayList;
import java.util.HashSet;

import usyd.comp3615.edia.db.Consumption;
import usyd.comp3615.edia.db.DatabaseHandler;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

public class RecipeAdapter extends ArrayAdapter<Consumption>{

	static class holder{
		TextView rName;
		Button quan;
		CheckBox tick;
	}

	private HashSet<Consumption> ingredients;
	private ArrayList<Consumption> meals;
	private final Context context;
	@SuppressWarnings("unused")
	private DatabaseHandler db;
	private Consumption c;

	public RecipeAdapter(Context context) {
		super(context, R.layout.result_row);
		this.context = context;
		db = new DatabaseHandler(this.context);
	}

	public RecipeAdapter(Context context, ArrayList<Consumption> meals, HashSet<Consumption> ingredients) {
		super(context, R.layout.result_row, meals);
		this.context = context;
		this.meals = meals;
		this.ingredients = ingredients;
		db = new DatabaseHandler(this.context);
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		
		holder h = null;
		if (v == null) {

			LayoutInflater inflater = ((Activity)context).getLayoutInflater();
			v = inflater.inflate(R.layout.recipe_select_row, parent, false);
			h = new holder();
			h.rName = (TextView) v.findViewById(R.id.recipeItemName);
			h.quan = (Button) v.findViewById(R.id.recipeItemQuantity);
			h.tick = (CheckBox) v.findViewById(R.id.recipeTick);

			v.setTag(h);
		}else{
			h = (holder) v.getTag();
		}
		c = meals.get(position);

		if (h == null){
			System.out.println("h is null");
		}
		else if (h.rName == null){
			System.out.println("rName is null");
		}
		h.rName.setText(c.getShortName());
		
		SpannableString quantityText = new SpannableString(c.getQuantity() + " grams");
		quantityText.setSpan(new UnderlineSpan(), 0, quantityText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		//UnderlineSpan underline = new UnderlineSpan();
		h.quan.setText(quantityText);
		
		h.tick.setChecked(true); // start all checkboxes as checked
		h.tick.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				Consumption d = meals.get(position);
				if (ingredients.contains(d)){
					ingredients.remove(d);
				}
				else{
					ingredients.add(d);
				}
				
				System.out.println("Current ingredients are: ");
				for (Consumption ingredient : ingredients){
					System.out.println(" - " + ingredient.getShortName());
				}
			}
		});
		
		h.quan.setOnClickListener(new Button.OnClickListener(){
			public void onClick(View arg0){
				Consumption d = meals.get(position);
				Intent intent = new Intent(context, QuantitySelector.class);
				Bundle b = new Bundle();				
				b.putInt("id", d.getSurveyID());
				b.putString("short_name", d.getShortName());
				b.putString("meal_type", d.getMealType());
				b.putSerializable("mode", QuantitySelector.Mode.SELECT); // we just want to fetch a quantity, and make no updates
				intent.putExtras(b);
				((Activity) context).startActivityForResult(intent, RequestCode.QUANTITY_SELECTION);
			}
		});

		return v;
	}
}