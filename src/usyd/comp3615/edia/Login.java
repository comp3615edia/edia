package usyd.comp3615.edia;

import java.util.concurrent.ExecutionException;

import usyd.comp3615.edia.db.DatabaseHandler;
import usyd.comp3615.edia.server.SendLogin;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
/**
 * Login deals with logging the user into the system.
 * @author COMP3615 Group Q edia (tomislav, michael, runi, callum, sam) 
 */
public class Login extends Activity{
	//All the fields from the xml.
	ImageButton btnLogin;
	Button btnLinkToRegister;
	EditText inputEmail;
	EditText inputPassword;
	TextView loginErrorMsg;
	
	String email, password;

	//Response strings
	//private static String KEY_SUCCESS = "success";
	//private static String KEY_ERROR = "error";
	//private static String KEY_ERROR_MSG = "error_msg";
	//private static String KEY_UID = "uid";
	//private static String KEY_NAME = "name";
	//private static String KEY_EMAIL = "email";
	//private static String KEY_CREATED_AT = "created_at";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);

		// Importing all assets like buttons, text fields
		inputEmail = (EditText) findViewById(R.id.loginEmail);
		inputPassword = (EditText) findViewById(R.id.loginPassword);
		btnLogin = (ImageButton) findViewById(R.id.btnLogin);
		btnLinkToRegister = (Button) findViewById(R.id.btnLinkToRegisterScreen);
		loginErrorMsg = (TextView) findViewById(R.id.login_error);

		// Login button Click Event
		btnLogin.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				email = inputEmail.getText().toString();
				password = inputPassword.getText().toString();
				
				try {
					
					connectToServer(email, password, view);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			//try {
			// user successfully logged in
			// Store user details in SQLite Database
			// Clear all previous data in database
			// Launch Dashboard Screen
			// Close all views before launching Dashboard
			// Close Login Screen
		});
	}
	
	/**
	 * Catches the result of the Registration and starts MainActivity on success (closing this intent).
	 * This ensures that pressing back on MainActivity will not return to log-in if you logged in through registering, but you can still return to log-in by pressing back from the registration page
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		if (requestCode == RequestCode.REGISTRATION){
			if (resultCode == RESULT_OK){
				switchMain(null);
			}
		}
	}
	
	
	//Switch to the registration screen.
	public void switchRegistration(View view){
		Intent intent = new Intent(this, Registration.class);
		startActivityForResult(intent, RequestCode.REGISTRATION);
	}
	//Switch to the main screen.
	public void switchMain(View view){
		Intent intent = new Intent(Login.this, MainActivity.class);
		startActivity(intent);
		finish();
	}
	
	public void connectToServer(String email, String password, View view) throws InterruptedException, ExecutionException {
		// Create a socket without a timeout
		
		// Send data to new thread in order to send to server
		String arr[] = {email, password};
		SendLogin sl = new SendLogin();
		sl.execute(arr);
		if (sl.get()) {
			Log.e("output",  "Successful Login");
			myToast("Successful Login");
			saveLogin(email);
			switchMain(view);
		}
		else {
			Log.e("output", sl.getMessage());
			myToast(sl.getMessage());
		}
	}
	
	private void saveLogin(String email) {
		DatabaseHandler db = new DatabaseHandler(this);
		db.login(email);
	}
	
	//A helper method to put a Toast object on the screen.
	public void myToast(String msg){
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
		inputPassword.setText("");
	}

}

/*
JSONArray jArr = new JSONArray();
JSONObject jObj = new JSONObject();
jObj.put("email", email);
jObj.put("password", password);
jArr.put(new JSONObject().put("type", "login"));
jArr.put(jObj);

// Send data to new thread in order to send to server
SendLogin sl = new SendLogin();
sl.execute(jArr);
if (sl.get()) {
	Log.e("output",  "Successful Login");
	switchMain(view);
}
else {
	Log.e("output", "Invalid Email or Password");
	//myToast("Invalid Email or Password");
}*/


