package usyd.comp3615.edia;

import java.util.List;

import usyd.comp3615.edia.db.Consumption;
import usyd.comp3615.edia.db.DatabaseHandler;
import usyd.comp3615.edia.db.Recipe;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
/**
 * The FavouritesAdapter changes the appearance of the ListView that displays the favourites.
 * It changes each row of the list view to appear like favourites_row.xml
 * 
 */
public class RecipeLoadAdapter extends BaseExpandableListAdapter{
	private List<Recipe> recipes;
	private Context _context;
	private DatabaseHandler db;

	public RecipeLoadAdapter(Context context, List<Recipe> recipes) {
		this._context = context;
		this.recipes = recipes;
		db = new DatabaseHandler(_context);
	}
	

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return getGroup(groupPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@SuppressLint("SimpleDateFormat")
	@Override
	public View getChildView(final int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		
		
		Recipe p = recipes.get(groupPosition);
		String ingredientGeneration = "This recipe consists of:\n";
		for (Consumption c : p.getItems()){
			ingredientGeneration += "- " + c.getQuantity() + " grams of " + c.getShortName() + "\n";
		}
		final String ingredients = ingredientGeneration;
		
		
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.recipe_row_info, null);
		}
		//Set up the description field
		TextView ingredientsView = (TextView) convertView.findViewById(R.id.ingredientsList);
		ingredientsView.setText(p.getRecipeDescription() + "\n" + ingredients);
		
		//Set up the remove from favourites button
		Button remove = (Button) convertView.findViewById(R.id.remove_recipe);
		
		
		remove.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0) {
				Recipe fitem = (Recipe) recipes.get(groupPosition);
				db.deleteRecipe(fitem);
				Intent intent = new Intent(_context,Recipes.class);
				((Activity) _context).setResult(2,intent);
				_context.startActivity(intent);
				((Activity)_context).finish();
			}
		});

		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return 1;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return recipes.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return recipes.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		final Recipe p = recipes.get(groupPosition);
		String ingredientGeneration = "This recipe consists of:\n";
		for (Consumption c : p.getItems()){
			ingredientGeneration += c.getQuantity() + " grams of " + c.getShortName();
		}
		final String ingredients = ingredientGeneration;
		
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.recipe_view_row, null);
		}

		TextView name = (TextView) convertView
				.findViewById(R.id.recipeName);
		name.setTypeface(null, Typeface.BOLD);
		name.setText(p.getRecipeName());
		
		ImageButton add = (ImageButton) convertView.findViewById(R.id.recipeAdd);
		add.setFocusable(false);

		add.setOnClickListener(new OnClickListener(){

			public void onClick(View arg0) {
				
				//Call the quantity selection with the needed input
				Intent intent = new Intent(_context, QuantitySelector.class);
				Bundle b = new Bundle();
				b.putBoolean("recipe", true);
				b.putString("ingredients", ingredients);
				b.putString("shortName", p.getRecipeName());
				b.putSerializable("mode", QuantitySelector.Mode.SELECT); //we want to add this amount on top of any existing consumptions of the same item in the same meal of the same day
				intent.putExtras(b);
				((Activity)_context).startActivityForResult(intent, RequestCode.QUANTITY_SELECTION);
			}
		});

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
}
