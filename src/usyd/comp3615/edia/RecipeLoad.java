package usyd.comp3615.edia;

import java.util.ArrayList;

import usyd.comp3615.edia.db.*;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.Toast;

/**
 * RecipeLoad deals with showing the recipes selected by the user and 
 * allowing the user to select them.
 * @author COMP3615 Group Q edia (tomislav, michael, runi, callum, sam)
 *
 */
public class RecipeLoad extends Activity{

	private ArrayList<Recipe> listItems = new ArrayList<Recipe>();
	private DatabaseHandler db;
	private ExpandableListView expListView;
	private Recipe fitem;
	private RecipeLoadAdapter adapter;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.recipe_load);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		//To fill the list view
		expListView = (ExpandableListView) findViewById(R.id.listRecipes);
		registerForContextMenu(expListView);
		getActionBar().setDisplayHomeAsUpEnabled(false);
		//Gets the strings to populate the list.
		populateList();

		adapter = new RecipeLoadAdapter(this, listItems);
		expListView.setAdapter(adapter);	

		// ExpandableListview Group expanded listener
		expListView.setOnGroupClickListener(new OnGroupClickListener() {

			public boolean onGroupClick(ExpandableListView arg0, View arg1,
					int groupPosition, long arg3) {
				return false;
			}
		});

		// ExpandableListview on child click listener
		expListView.setOnChildClickListener(new OnChildClickListener() {

			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				return false;
			}
		});

		expListView.setOnItemLongClickListener(new OnItemLongClickListener() {

			int childPosition,groupPosition;

			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				int itemType = ExpandableListView.getPackedPositionType(id);

				if ( itemType == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
					//TODO Is childPosition needed?
					childPosition = ExpandableListView.getPackedPositionChild(id);
					groupPosition = ExpandableListView.getPackedPositionGroup(id);

					// At the moment we don't want anything to happen when the child
					// is long clicked, that might change. 
					return false; //true if we consumed the click, false if not

				} else if(itemType == ExpandableListView.PACKED_POSITION_TYPE_GROUP) {
					groupPosition = ExpandableListView.getPackedPositionGroup(id);
					fitem = listItems.get(groupPosition);
					CharSequence[] choices = new CharSequence[1];
					choices[0] = "Remove " + fitem.getRecipeName() + " from Recipes";
					//Open a menu to select delete
					AlertDialog.Builder b = new AlertDialog.Builder(RecipeLoad.this);
					b.setTitle(fitem.getRecipeName()+ " menu");
					b.setItems(choices, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							if(which==0){
								myToast("deleted");
								db.deleteRecipe(fitem);
								listItems.remove(fitem);
							}
						}
					});
					AlertDialog menu = b.create();
					menu.show();
					return true; //true if we consumed the click, false if not

				} else {
					// null item; we don't consume the click
					return false;
				}
			}
		});
	}

	/**
	 * populateList is a helper function to fill the favourites list from the file/database.
	 */
	public void populateList(){
		listItems.clear();
		db = new DatabaseHandler(this);
		//Get all of the favourites from the database.
		listItems.addAll(db.getAllRecipes());
		System.out.println("There are " + listItems.size() + " recipes");
		ArrayList<Consumption> ingredients = new ArrayList<Consumption>();
	
	//	listItems.add(new FavouriteItem(12201018,"Bagel, from white flour", "A ring of baked yeast dough with a chewy centre and a crusty outside. It is typically 8-10cm in diameter. Usually poached in water or steamed before being baked. Ready to eat.","Bagel"));
	}

	public void myToast(String msg){
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
	}

	/**
	 * Closes this activity if the QuantitySelection was successful, so that hitting back from MealActivity goes to MainActivity
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		if (requestCode == RequestCode.QUANTITY_SELECTION){
			if (resultCode == RESULT_OK){
 				int amount = data.getIntExtra("quantity", 0);
				
				Intent intent = new Intent(this, MealActivity.class);
				startActivity(intent);
				finish();
			}
		}
		adapter.notifyDataSetChanged();
	}
}
