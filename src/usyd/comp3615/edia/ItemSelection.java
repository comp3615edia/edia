package usyd.comp3615.edia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import usyd.comp3615.edia.db.CSVParser;
import usyd.comp3615.edia.db.DatabaseHandler;
import usyd.comp3615.edia.db.ItemTree;
import usyd.comp3615.edia.db.ItemTree.Node;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class ItemSelection extends Activity {
	
	
	private class NodeClickableSpan extends ClickableSpan{

		public void onClick(View widget) {
						
			currentNode = nodeHistory.get(this);
			ambiguous = ambiguityHistory.get(currentNode);
			System.out.println("We are now sitting at node " + currentNode.getFullName() + ", ambiguous = "  + ambiguous);
			
			//refresh the title
			String currentNodeName = currentTitle.subSequence(currentTitle.getSpanStart(this), currentTitle.getSpanEnd(this)).toString();
			currentTitle = (SpannableStringBuilder) currentTitle.subSequence(0, currentTitle.getSpanStart(this));
			currentTitle.append(currentNodeName);
			text.setText(currentTitle);
			
			//update the name of our last choice
			lastChoiceName = currentNodeName;
			System.out.println("Last choice name is now " + lastChoiceName);
			loadContents(false);
		}
	}
	
	Context _context; //used to access 'this' inside anonymous inner classes
	String mealType;
	
	HashMap<Node, Integer> commonNodeSizes; //in the even of ambiguity, the number of results is not equal to the subtree size (since only some subtree paths are relevant) so we need to find and store new sizes instead
	private ItemTree.Node currentNode;
	private ItemTree.Node lastNode;
	SpannableStringBuilder currentTitle;
	private String result; // the abbreviated name that was passed into this activity
	private TextView text;
	private ListView categoryList;
	private DatabaseHandler db;
	List<Node> categoryOptions;
	ArrayList<String> categoryNames = new ArrayList<String>();
	private Button submitBtn;
	Map<NodeClickableSpan, ArrayList<ItemTree.Node>> historyMap; // maps the clickable spans to a list of nodes, which were the categories shown on the screen
	Map<NodeClickableSpan, ItemTree.Node> nodeHistory;
	Map<ItemTree.Node, Boolean> ambiguityHistory;
	Boolean ambiguous = false;
	SearchAdapter adapter;
	Boolean lastAmbiguous = false;
	String lastChoiceName = "";
	String title;
	int newSpanStart;
	private HashSet<ItemTree.Node> possibleStarts = new HashSet<ItemTree.Node>();
	HashSet<ItemTree.Node> commonNodes; // only used in the case of ambiguity, but needs to be class-accesible to be used in the anonymous inner classes!
	HashSet<ItemTree.Node> visitedNodes;
	QuantitySelector.Mode mode;
	
	ArrayList<Integer> numResults = new ArrayList<Integer>();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.item_selection);

		// initialize variables
		text = (TextView) findViewById(R.id.PhysicalLevelTitle);
		submitBtn = (Button) findViewById(R.id.buttonSedentary);
		submitBtn.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0){
				//not sure what purpose the submit button serves?
			}
		});
		
		_context = this;
		db = new DatabaseHandler(_context);
		Bundle b = this.getIntent().getExtras();
		mealType = b.getString("meal_type"); // breakfast/lunch/dinner etc.
		result = b.getString("result"); // the name as shown to the user that was pressed
		mode = (QuantitySelector.Mode) b.getSerializable("mode");
		if (mealType == null || result == null){
			finish();
		}
		nodeHistory = new HashMap<NodeClickableSpan, ItemTree.Node>();
		ambiguityHistory = new HashMap<ItemTree.Node, Boolean>();
		adapter = new SearchAdapter(this, categoryNames, numResults);
		categoryList = (ListView) findViewById(R.id.listView1);
		categoryList.setAdapter(adapter);
		currentTitle = new SpannableStringBuilder(result);
		
		
		final ArrayList<String> possibleStartNames = b.getStringArrayList("nodeNames"); //this refers to all nodes relevant to the search query that we have to decide from (eg. peeled apple could be either green skin or red skin!)
		
		//if theres only one node option, we can just select it, otherwise we need to resolve ambiguity first
		if (possibleStartNames.size() == 1){			
			ambiguous = false;
			System.out.println(possibleStartNames.get(0));
			
			// This line crashes
			currentNode = ItemTree.getInstance().findNode(possibleStartNames.get(0));
			
			//handle errors 
			if (currentNode == null){
				System.err.println("Node does not exist, remaking tree");	
				ItemTree.getInstance().init(db);
				currentNode = ItemTree.getInstance().findNode(possibleStartNames.get(0));
			}
			else if (currentNode.getChildren() == null){
				System.err.println("Node has null children");
			}
			
			//move down the tree until either there are multiple options or we land on an item
			while (currentNode.getChildren().size() == 1 && currentNode.getItem() == null){
				currentNode = currentNode.getChildren().get(0);
			}
			
			//if the subtree size is 1 now, we must be sitting on the only possible option - this is because a node with multiple children will have size > 1, as would a node that has children but is also an item itself
			if (currentNode.getSubtreeSize() == 1){
				getQuantity();
				return;
			}
			else{
				//if we aren't returning right away, we'll need a list of categories and their sizes!
				categoryOptions = currentNode.getChildren();
				for (Node child : categoryOptions){
					categoryNames.add(child.getName());
					numResults.add(child.getSubtreeSize()); // in a non-ambiguous situation, the entire subtree of this node is relevant to our search 
				}
				if (currentNode.getItem() != null){ //if this item itself has an item, it also needs to be added to the list
					categoryNames.add("not further specified");
					numResults.add(1);
				}
				
			}
		} else {
			ambiguous = true;
			commonNodes = new HashSet<ItemTree.Node>();
			visitedNodes = new HashSet<ItemTree.Node>();		
			commonNodeSizes = new HashMap<Node, Integer>();
			
			//find all the nodes that our possible starts have in common (there will be at least one, since they will be of the same base type)	
			for (String node : possibleStartNames){
				Node startingNode = ItemTree.getInstance().findNode(node);
				possibleStarts.add(startingNode);
				
				Node matchingNode = startingNode;
				//keep a track of all the nodes required passed to get to this node
				while(matchingNode.getParent() != null){ //we don't want to visit the root, because obviously all paths include the root and this shouldn't count as a clash
					Integer subtreeSize;
					if ((subtreeSize = commonNodeSizes.get(matchingNode)) == null){
						subtreeSize = 0;	
					}
					commonNodeSizes.put(matchingNode, subtreeSize + startingNode.getSubtreeSize());
					if (!visitedNodes.contains(matchingNode)){
						visitedNodes.add(matchingNode);
					}
					else{
						//if we've visited this node before, mark it clash - these are the nodes that could possibly be used to resolve the ambiguity
						currentNode = matchingNode; //by the end of this, currentNode will be the highest point in the tree that the ambiguity occurs 
						if (!commonNodes.contains(matchingNode)){
							commonNodes.add(matchingNode);
						}
					}
					matchingNode = matchingNode.getParent();
				}
			}
			
			//now we move back down the tree to skip irrelevant options
			ArrayList<ItemTree.Node> relevantNodes = new ArrayList<ItemTree.Node>();
			relevantNodes.add(currentNode);
			do{
				currentNode = relevantNodes.get(0);
				relevantNodes.clear();
				for (ItemTree.Node child : currentNode.getChildren()){
					if (visitedNodes.contains(child)){
						relevantNodes.add(child);
					}
				}		
			} while (relevantNodes.size() == 1 && currentNode.getItem() == null); //stop when the current node can be used to resolve ambiguity, which is a split between two visited paths 
			 
			
			//now make the list of options - if any of these children are one of our starting points, skip it and show its children instead
			categoryOptions = new ArrayList<ItemTree.Node>();
			for (ItemTree.Node child : relevantNodes){	
				if (possibleStarts.contains(child)){
					for (ItemTree.Node grandChild : child.getChildren()){
						categoryOptions.add(grandChild);
						categoryNames.add(grandChild.getName());
						numResults.add(grandChild.getSubtreeSize());
					}
					//add the node itself only if it corresponds to an item!
					if (child.getItem() != null){
						categoryOptions.add(child);
						categoryNames.add("not further specified");
						numResults.add(child.getSubtreeSize());
					}
				}
				else{
					//all remaining nodes are those that resolve ambiguity - the size we are interested in is only a fraction of the nodes actual size, so use commonNodeSizes instead
					categoryOptions.add(child);
					categoryNames.add(child.getName());
					System.out.println("Adding " + commonNodeSizes.get(child) + " as subtree size for " + child.getName());
					numResults.add(commonNodeSizes.get(child));
				}
			}
		}
		
		text.setText(currentTitle);
		lastChoiceName = result; //if we reach this point, there is actually going to be some category selection required, so we need to keep track of our last choice (which starts at our current point)	
		adapter.notifyDataSetChanged(); //display our results on the view
	
		categoryList.setOnItemClickListener(new OnItemClickListener(){
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				//save our current state as the last state so that we can return if this ends in a QuantitySelection that is called
				lastNode = currentNode; //keep track of where we were before we started moving so we can get back here if the quantity selector is cancelled
				lastAmbiguous = ambiguous;
				
				System.out.println("setting last node = " + lastNode.getName());
				System.out.println("setting last ambiguous = " + lastAmbiguous);
				
				//find the start of the text we'll have to remove before we change lastChoiceName
				title = currentTitle.toString();
				newSpanStart = title.lastIndexOf(lastChoiceName);
				
				//move to the pressed item 
				if (position == categoryOptions.size()){ //if we are selecting an item that isn't in the list, it must be "not further specified", which would mean we return the node we're currently on
					lastChoiceName = "not further specified";
					getQuantity();
					return;
				}
				else{
					currentNode = categoryOptions.get(position);					
					lastChoiceName = currentNode.getName();
					//find out if we're still ambiguous
					if (possibleStartNames.contains(currentNode.getParent())){
						ambiguous = false;
					}
					System.out.println("We are now sitting at node " + currentNode.getFullName() + ", ambiguous = "  + ambiguous);
				}
				
				loadContents(true);
			}
		});
	}
	/**
	 * Catches the result of the QuantitySelection that is called when an item is selected, and then lets MealActivity know that the whole process was successful
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		if (requestCode == RequestCode.QUANTITY_SELECTION){
			if (resultCode == RESULT_OK){
				Intent returnIntent = new Intent();
				if (mode == QuantitySelector.Mode.SELECT){
					returnIntent.putExtra("id", data.getIntExtra("id", 0));
					returnIntent.putExtra("short_name", data.getStringExtra("short_name"));
					returnIntent.putExtra("quantity", data.getIntExtra("quantity", 0));
				}
				setResult(RESULT_OK, returnIntent);
				finish();
			}
			else{
				//jump back to the node that came before this selection
				backtrack();
			}
		}
	}

	private void getQuantity(){
		Intent intent = new Intent(this, QuantitySelector.class);
		Bundle b = new Bundle();
	
		b.putInt("id", currentNode.getItem().getID());
		b.putString("short_name", currentTitle.toString() + (lastChoiceName.equals("")? "" : ", " + lastChoiceName)); //now that the activity has been called, we need to adjust this variable in case the user returns
		
		String title = currentTitle.toString();
		
		List<String> selectionTiers = CSVParser.parse(currentTitle.toString());
		lastChoiceName = selectionTiers.get(selectionTiers.size() - 1);
		newSpanStart = title.lastIndexOf(lastChoiceName);
		
		if (mode == null) mode = QuantitySelector.Mode.ADD; //set add as the default action
		b.putSerializable("mode", mode); //tell the quantity selection that we are adding the result on top of any already existing consumptions
		b.putString("meal_type", MealActivity.currentMealType);
		intent.putExtras(b);
		startActivityForResult(intent, RequestCode.QUANTITY_SELECTION);
	}
	
	private void backtrack(){
		//this occurs when the MealActivity search goes directly to the QuantitySelector, with no category selection required - in this case, we just want to return to search
		if (lastNode == null){
			Intent intent = new Intent(this,MealActivity.class);
			this.setResult(RESULT_CANCELED,intent);
			finish();
		} else{
			//revert to the way we were without updating the title
			currentNode = lastNode;
			ambiguous = lastAmbiguous;
			System.out.println("Just backtracked, now currentNode = " + currentNode.getFullName() + ", ambiguity = " + ambiguous + " and lastChoiceName = " + lastChoiceName);
			loadContents(false);
		}
	}
	
	private void loadContents(boolean updateTitle){
		
		//clear all our lists
		categoryNames.clear();
		numResults.clear();
		
		//move down until we find something relevant again
		if (ambiguous){
			categoryOptions = new ArrayList<Node>();
			categoryOptions.add(currentNode);
			do{
				currentNode = categoryOptions.get(0);
				categoryOptions.clear();
				for (ItemTree.Node child : currentNode.getChildren()){							
					if (visitedNodes.contains(child)){
						categoryOptions.add(child);
					}
				}		
			} 
			while (categoryOptions.size() == 1 && currentNode.getItem() == null); //stop when the current node can be used to resolve ambiguity
			
			if (categoryOptions.size() == 0){
				System.out.println("no longer ambiguous");
				ambiguous = false;
			} 
			else{
				//if any of these children are one of our starting points, skip it and show its children instead
				ArrayList<Node> relevantNodes = new ArrayList<Node>();
				for (ItemTree.Node child : categoryOptions){
					if (possibleStarts.contains(child)){
						for (ItemTree.Node grandChild : child.getChildren()){
							relevantNodes.add(grandChild);
							categoryNames.add(grandChild.getName());
							numResults.add(grandChild.getSubtreeSize());
						}
						if (child.getItem() != null){
							relevantNodes.add(child);
							categoryNames.add("not further specified");
							numResults.add(child.getSubtreeSize());
						}
					}
					else{
						relevantNodes.add(child);
						categoryNames.add(child.getName());
						numResults.add(commonNodeSizes.get(child));
					}
				}
				categoryOptions = relevantNodes;
			}
		}
		
		// use a seperate if instead of an else, since ambiguity may have been resolved!
		if (!ambiguous){	
			
			System.out.println("Not ambiguous");
			
			//move down the tree until either there are multiple options or we land on an item
			while (currentNode.getChildren().size() == 1 && currentNode.getItem() == null){
				currentNode = currentNode.getChildren().get(0);
			}
			
			System.out.println("Skipping irrelevant nodes, now at " + currentNode.getFullName());

			//if the subtree size is 1 now, we must be sitting on the only possible option - this is because a node with multiple children will have size > 1, as would a node that has children but is also an item itself
			if (currentNode.getSubtreeSize() == 1){
				getQuantity();
				return;
			}
			else{
				categoryOptions = currentNode.getChildren();
				System.out.println("This node has " + categoryOptions.size() + " children");
				for (Node child : categoryOptions){
					categoryNames.add(child.getName());
					numResults.add(child.getSubtreeSize()); // in a non-ambiguous situation, the entire subtree of this node is relevant to our search 
				}
				if (currentNode.getItem() != null){ //if this item itself has an item, it also needs to be added to the list
					categoryNames.add("not further specified");
					numResults.add(1);
				}
			}		
		}	
		
		System.out.println("After load contents, we are sitting at " + currentNode.getFullName() + " and ambiguous = " + ambiguous);
		
		if (updateTitle){
			//change the title by removing the last added text
			currentTitle = (SpannableStringBuilder) currentTitle.subSequence(0, newSpanStart);
			String removedText = title.substring(newSpanStart); 
			
			//re-add the text we just removed from the title, but make it clickable this time!	
			NodeClickableSpan clickableSpan =  new NodeClickableSpan();
			SpannableString ss = new SpannableString(removedText);
			ss.setSpan(new UnderlineSpan(), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			ss.setSpan(clickableSpan, 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			currentTitle.append(ss);		
			
			//now add on our last choice name in plain text and update
			currentTitle.append(", " + lastChoiceName);
			text.setText(currentTitle);
			text.setMovementMethod(LinkMovementMethod.getInstance());
			
			//attach the node and ambiguity to the clickable span using our maps 
			nodeHistory.put(clickableSpan, lastNode);
			ambiguityHistory.put(lastNode, lastAmbiguous);
			System.out.println("Put " + lastNode.getName() + " into the map attached to " + removedText + ", ambiguous = " + lastAmbiguous);
		}
		
		adapter.notifyDataSetChanged();	
	}
}