
package usyd.comp3615.edia;

/**
 * Used by startActivityForResult and onActivityResult to trace which activity is returning, so that we can have unique reactions depending on what activity is returning
 * @author CJR
 */
public class RequestCode{
	public static final int MEAL = 1;
	public static final int FAVOURITES = 2;
	public static final int ITEM_SELECTION = 3;
	public static final int QUANTITY_SELECTION = 4;
	public static final int LOGIN = 4;
	public static final int SETTINGS = 5;
	public static final int REGISTRATION = 6;
	public static final int RECIPE = 7;
	public static final int SEARCH_LIST = 8;
}