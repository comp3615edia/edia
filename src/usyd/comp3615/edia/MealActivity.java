package usyd.comp3615.edia;

import java.util.ArrayList;

import usyd.comp3615.edia.db.Consumption;
import usyd.comp3615.edia.db.DatabaseHandler;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.support.v4.app.NavUtils;

/**
 * MealActivity deals with handling the food search and selection
 * @author COMP3615 Group Q edia (tomislav, michael, runi, callum, sam)
 *
 */
public class MealActivity extends Activity{

	//moved to searchlist
//	final String[] suffixKeywords = new String[]{"added ", "contains ", "no ", "with ", "sandwiched ", "coated ", "fried "};
//	private final int RESULT_LIMIT = 50;
//	private final int MIN_QUERY_LENGTH = 3;
//	private final int NUM_DAYS_RECENT = 5; //number of days until an item is no longer considered recent
//	// -- end move
	
	public static String currentMealType = "";
	public static final String BREAKFAST = "breakfast";
	public static final String LUNCH = "lunch";
	public static final String DINNER = "dinner";
	public static final String SNACK = "snack";

	//moved to searchlist
//	private List<String> listItems = new ArrayList<String>(); //the "short names" of the items (eg. peeled apple) which is shown in search results
//	private HashMap<String, ArrayList<String>> associatedNodePaths = new HashMap<String, ArrayList<String>>(); // a single name in the list could be referring to several different items (eg. peeled apple could be red or green skin) so 
//	private ArrayAdapter<String> adapter;
	// --end move
	
	private ArrayList<Consumption> meals = new ArrayList<Consumption>();
	private MealAdapter rAdapter;
	private DatabaseHandler db = new DatabaseHandler(this); 
	private ListView listView2;
	private TextView title;
	//private FavouriteItem fitem;
//	ItemTree itemTree;
	//private String item;
	//private SearchView srcTom; // moved to searchlist

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_meal);
		
		//This is used to set the current meal type
		//TODO Need to use this String to filter database for relevant entries
		//Intent intent = getIntent();
		
		title = (TextView) findViewById(R.id.mealTitle);
		title.setText(currentMealType);
		
		try{
			meals = db.getAllConsumptionItems(0, currentMealType);
		}catch (NullPointerException e){
			e.printStackTrace();
		}
		System.out.println("Entered meal activity as meal " + currentMealType);
		
		// Show the Up button in the action bar.
		setupActionBar();
		//Display the already selected items
		listView2 = (ListView) findViewById(R.id.resultList);
		rAdapter = new MealAdapter(this,meals, "MEALACTIVITY");
		listView2.setAdapter(rAdapter);	
		registerForContextMenu(listView2);
		
		ImageButton favourites = (ImageButton) findViewById(R.id.addFromFavourites);
		favourites.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0) {
				Intent favIntent = new Intent(MealActivity.this, Favourites.class);
				favIntent.putExtra("meal_type", currentMealType);
				startActivityForResult(favIntent, RequestCode.FAVOURITES);
			}
		});
		
		Button searchButton = (Button) findViewById(R.id.button1);
		searchButton.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0) {
				Intent searchIntent = new Intent(MealActivity.this, SearchList.class);
				searchIntent.putExtra("meal_type", currentMealType);
				searchIntent.putExtra("mode", QuantitySelector.Mode.ADD);
				startActivityForResult(searchIntent, RequestCode.SEARCH_LIST);
			}
		});
		
		
		/**
		 * Recipes not functioning properly as of release, and so links were removed
		 * Database tables, objects and activities are provided so that future updates can be made
		 */
//		ImageButton loadRecipe = (ImageButton) findViewById(R.id.importRecipeButton);
//		loadRecipe.setOnClickListener(new Button.OnClickListener(){
//			public void onClick(View v) {
//				Intent loadIntent = new Intent(MealActivity.this, RecipeLoad.class);
//				startActivityForResult(loadIntent, RequestCode.RECIPE);
//			}
//		});
		
//		ImageButton addRecipe = (ImageButton) findViewById(R.id.translateButton);
//		addRecipe.setOnClickListener(new OnClickListener(){
//			public void onClick(View arg0) {
//				if (meals.size() == 0) {
//					Toast.makeText(getApplicationContext(), "No foods consumed", Toast.LENGTH_LONG).show();
//					return;
//				}
//				Intent i = new Intent(MealActivity.this,Recipes.class);
//				i.putExtra("meal_type", currentMealType);
//				startActivityForResult(i,RequestCode.RECIPE);
//			}
//		});
		
		ImageButton completeButton = (ImageButton) findViewById(R.id.completeButton);
		completeButton.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0) {
				finish();
			}
		});
		
		

//		listView.setOnItemClickListener(new OnItemClickListener() {
//			@Override
//			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//				item = ((TextView)view).getText().toString();
//				ArrayList<String> associatedNodes = associatedNodePaths.get(listItems.get(position));		
//				Bundle b = new Bundle();				
//				b.putString("result", item);
//				b.putString("meal_type", currentMealType);
//				//ItemSelection is ADD mode, so no need to set
//				b.putStringArrayList("nodeNames", associatedNodes);
//				Intent intent = new Intent(MealActivity.this, ItemSelection.class);
//				intent.putExtras(b);
//				startActivityForResult(intent, RequestCode.ITEM_SELECTION);
//			}
//		});
		
//		itemTree = ItemTree.getInstance();
//		srcTom.setQuery("", false); //start the search with an empty string (which will load recently used items)
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		//getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	/**
	 * Catches the result of the ItemSelection that is called when a search result is pressed.
	 * For a consumption to be registered, it must go through both a successful ItemSelection AND a successful QuantitySelection.
	 * 
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		
		if (requestCode == RequestCode.FAVOURITES || ((requestCode == RequestCode.SEARCH_LIST || requestCode == RequestCode.QUANTITY_SELECTION) && resultCode == RESULT_OK)){
			meals.clear();
			meals.addAll(db.getAllConsumptionItems(0, currentMealType));
			if (requestCode == RequestCode.FAVOURITES)
				finish();
			rAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.meal, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
