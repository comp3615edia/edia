package usyd.comp3615.edia.ui;

import java.util.Calendar;

import usyd.comp3615.edia.Registration;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

public class DatePickerFragment extends DialogFragment
implements DatePickerDialog.OnDateSetListener {

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		boolean forceShowDialog = savedInstanceState==null;
        boolean showsDialog = getShowsDialog();
        super.onCreate(savedInstanceState);
        if (forceShowDialog )
            setShowsDialog(showsDialog);
		// Use the current date as the default date in the picker
		final Calendar c = Calendar.getInstance();
		c.add(Calendar.YEAR, -21);
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);

		// Create a new instance of DatePickerDialog and return it
		return new DatePickerDialog(getActivity(), this, year, month, day);
	}

	public void onDateSet(DatePicker view, int year, int month, int day) {
		Registration.setDate(day,month,year);
	}
}

