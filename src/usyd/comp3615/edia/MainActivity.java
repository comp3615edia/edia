package usyd.comp3615.edia;

import java.util.Calendar;

import usyd.comp3615.edia.CustomPageAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.View;


public class MainActivity extends FragmentActivity
{
	public static final String EXTRA_MESSAGE = "usyd.comp3615.edia.MESSAGE";
	public final static int PAGES = 3;
	public final static int LOOPS = 1000; 
	public final static int FIRST_PAGE = PAGES * LOOPS / 2;
	public final static int SECOND_PAGE = PAGES * LOOPS / 2 + 1;
	public final static int THIRD_PAGE = PAGES * LOOPS / 2 + 2;
	public final static float BIG_SCALE = 1.0f;
	public final static float SMALL_SCALE = 0.8f;
	public final static float DIFF_SCALE = BIG_SCALE - SMALL_SCALE;
	
	public CustomPageAdapter adapter;
	public ViewPager pager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		pager = (ViewPager) findViewById(R.id.customviewpager);

		adapter = new CustomPageAdapter(this, this.getSupportFragmentManager());
		pager.setAdapter(adapter);
		pager.setOnPageChangeListener(adapter);
		
		pager.setCurrentItem(getTypeOfMeal());
		
		pager.setOffscreenPageLimit(3);
		pager.setPageMargin(-200);
	}
	
	/**
	 * Calculates the current time of the day and selects the type of meal
	 * @return the type of meal. 1: breakfast, 2: lunch, 3: dinner.
	 */
	private int getTypeOfMeal() {
		Calendar c = Calendar.getInstance();
		int time = c.get(Calendar.HOUR_OF_DAY);
		if (time >= 2 && time < 12)
			return FIRST_PAGE;
		else if (time >= 12 && time < 16)
			return SECOND_PAGE;
		else if (time >= 16 || time < 2)
			return THIRD_PAGE;
		else
			return FIRST_PAGE;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_activity_actions, menu);
		return super.onCreateOptionsMenu(menu);
	}

//	public void goToFavourites(View view){
//		Intent intent = new Intent(MainActivity.this, Favourites.class);
//		switch (getTypeOfMeal()) {
//			case FIRST_PAGE:
//				MealActivity.currentMealType = MealActivity.BREAKFAST;
//				break;
//			case SECOND_PAGE:
//				MealActivity.currentMealType = MealActivity.LUNCH;
//				break;
//			case THIRD_PAGE:
//				MealActivity.currentMealType = MealActivity.DINNER;
//				break;
//			default:
//				MealActivity.currentMealType = MealActivity.SNACK;
//				break;
//		}
//		startActivity(intent);
//	}
	
	public void snacksBar(View view) {
		Intent intent = new Intent(MainActivity.this, MealActivity.class);
		MealActivity.currentMealType = MealActivity.SNACK;
		startActivity(intent);
	}

}
