package usyd.comp3615.edia.server;

import java.io.IOException;
import java.io.OutputStreamWriter;

import java.net.HttpURLConnection;
import java.net.URL;

import android.os.AsyncTask;

public class SendLogin extends AsyncTask<String, Void, Boolean> {

	private String message = "Welcome to EDIA";
	@Override
	protected Boolean doInBackground(String... args) {
	
		URL url = null;
		try {
			url = new URL(Server.SITE_LOGIN);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			
			con.setDoOutput(true);
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", Server.USER_AGENT);
			con.setRequestProperty("Accept-Language",  "en-US,en;q=0.5");
			con.setConnectTimeout(1500);

			OutputStreamWriter os = new OutputStreamWriter(con.getOutputStream());

			os.write("email="+args[0]+"&password="+args[1]);

			os.flush();
			int code = con.getResponseCode();
			con.disconnect();
			if (code == 203)
				return true;
			else {
				message = "Invalid Email or Password";
				return false;
			}

		} catch (IOException e) {
			message = "No internet service available";
		}
		return false;
	}
	
	public String getMessage() {
		return message;
	}

}
