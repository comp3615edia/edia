package usyd.comp3615.edia.server;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import usyd.comp3615.edia.db.Consumption;
import usyd.comp3615.edia.db.DatabaseHandler;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class LogService extends Service {

	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		DatabaseHandler db = new DatabaseHandler(this);
		
		
		System.out.println("Started IntentService");
		ArrayList<Consumption> list;
		String lastCommit = db.getLatestCommit();
		if (lastCommit.length() <= 0) {
			System.out.println("this executed");
			list = db.getAllConsumptionItems(1, "all");			
		}
		else {
			@SuppressWarnings("deprecation")
			Date d1 = new Date(lastCommit);
			@SuppressWarnings("deprecation")
			Date d2 = new Date(dateFormat.format(Calendar.getInstance().getTime()));
			int num = (int) ((d2.getTime() - d1.getTime())/1000/60/60/24);
			if (num == 0) return Service.START_NOT_STICKY;
			System.out.println(num);
			list = new ArrayList<Consumption>();
			for (int i = num; i > 0; i--)
				list.addAll(db.getAllConsumptionItems(i, "all"));
			
			System.out.println(list.size());
		}
		
		//SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		
		Consumption arr[] = new Consumption[list.size()];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = list.get(i);
		}

		SendLog sl = new SendLog(this);
		sl.execute(arr);
		try {
			if (sl.get()) {
				db.addCommit(dateFormat.format(Calendar.getInstance().getTime()));
				Log.e("output",  "Successful Transfer");
				System.out.println("Successful Transfer");
			}
			else {
				Log.e("output", "Failed Transfer");
				System.out.println("Failed Transfer");
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return Service.START_NOT_STICKY;
		
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

}
