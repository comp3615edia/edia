package usyd.comp3615.edia.server;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import usyd.comp3615.edia.db.Consumption;
import usyd.comp3615.edia.db.DatabaseHandler;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

public class SendLog extends AsyncTask<Consumption, Void, Boolean> {

	private String message = null;
	private LogService service;
	
	public SendLog(LogService service) {
		this.service = service;
	}
	@Override
	protected Boolean doInBackground(Consumption... args) {
		DatabaseHandler db = new DatabaseHandler(service);
		URL url = null;
		Consumption item = null;
		Log.e("Server", "Sending data from User: " + DatabaseHandler.USER);
		if (!db.isLoggedIn()) {
			Log.e("Server", "User is not logged in. Can't send data");
			System.out.println("not logged in");
			return false;
		}
		
		for (int i = 0; i < args.length; i++) {
			item = args[i];
			try {
				url = new URL(Server.SITE_LOG);
				HttpURLConnection con = (HttpURLConnection) url.openConnection();
				
				con.setDoOutput(true);
				con.setRequestMethod("POST");
				con.setRequestProperty("User-Agent", Server.USER_AGENT);
				con.setRequestProperty("Accept-Language",  "en-US,en;q=0.5");
		
				OutputStreamWriter os = new OutputStreamWriter(con.getOutputStream());
				
				String str = "email="+DatabaseHandler.USER+
						"&survey_id="+item.getSurveyID() +
						"&quantity="+item.getQuantity() +
						"&time_eaten="+item.getTime_eaten() + 
						"&location="+item.getLocation() +
						"&meal_type="+item.getMealType();
				os.write(str);
		
				os.flush();
				int code = con.getResponseCode();
				con.disconnect();
				
				if (code != 203) {
					System.out.println("Successfully sent data item: " + item.getSurveyID());
					continue;
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	public String getMessage() {
		return message;
	}
	
	public boolean hasActiveInternetConnection() {
	    if (isNetworkAvailable()) {
	        try {
	            HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
	            urlc.setRequestProperty("User-Agent", "Test");
	            urlc.setRequestProperty("Connection", "close");
	            urlc.setConnectTimeout(1500); 
	            urlc.connect();
	            return (urlc.getResponseCode() == 200);
	        } catch (IOException e) {
	            Log.e("service", "Error checking internet connection", e);
	        }
	    } else {
	        Log.d("service", "No network available!");
	    }
	    return false;
	}
	
	private boolean isNetworkAvailable() {
	    ConnectivityManager connectivityManager 
	         = (ConnectivityManager) service.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null;
	}

}
