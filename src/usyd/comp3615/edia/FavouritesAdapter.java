package usyd.comp3615.edia;

import java.util.List;

import usyd.comp3615.edia.db.DatabaseHandler;
import usyd.comp3615.edia.db.FavouriteItem;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
/**
 * The FavouritesAdapter changes the appearance of the ListView that displays the favourites.
 * It changes each row of the list view to appear like favourites_row.xml
 * 
 */
public class FavouritesAdapter extends BaseExpandableListAdapter{
	private List<FavouriteItem> favourites;
	private Context _context;
	private DatabaseHandler db;

	public FavouritesAdapter(Context context, List<FavouriteItem> favourites) {
		this._context = context;
		this.favourites = favourites;
		db = new DatabaseHandler(_context);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return getGroup(groupPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@SuppressLint("SimpleDateFormat")
	@Override
	public View getChildView(final int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		FavouriteItem p = favourites.get(groupPosition);

		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.favourite_row, null);
		}
		//Set up the description field
		TextView desc = (TextView) convertView.findViewById(R.id.favouriteDescription);
		desc.setText(p.getDescription());

		//Set up the remove from favourites button
		Button remove = (Button) convertView.findViewById(R.id.favourite_remove);

		remove.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0) {
				FavouriteItem fitem = (FavouriteItem) favourites.get(groupPosition);
				db.deleteFavourite(fitem);
				Intent intent = new Intent(_context,Favourites.class);
				((Activity) _context).setResult(2,intent);
				_context.startActivity(intent);
				((Activity)_context).finish();
			}
		});

		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return 1;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return favourites.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return favourites.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		final FavouriteItem p = favourites.get(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.favourite_row_header, null);
		}

		TextView favouriteSName = (TextView) convertView
				.findViewById(R.id.favouriteShortName);
		favouriteSName.setTypeface(null, Typeface.BOLD);
		favouriteSName.setText(p.getFavouriteShortName());

		TextView favouriteName = (TextView) convertView
				.findViewById(R.id.favouriteName);
		favouriteName.setText(p.getName());

		ImageButton add = (ImageButton) convertView.findViewById(R.id.favouriteAdd);
		add.setFocusable(false);

		add.setOnClickListener(new OnClickListener(){

			public void onClick(View arg0) {
				//Call the quantity selection with the needed input
				Intent intent = new Intent(_context, QuantitySelector.class);
				Bundle b = new Bundle();
				b.putInt("id", p.getID());
				b.putString("short_name", p.getFavouriteShortName());
				b.putString("meal_type", MealActivity.currentMealType);
				b.putSerializable("mode", QuantitySelector.Mode.ADD); //we want to add this amount on top of any existing consumptions of the same item in the same meal of the same day
				intent.putExtras(b);
				((Activity)_context).startActivityForResult(intent, RequestCode.QUANTITY_SELECTION);
			}
		});

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
}
