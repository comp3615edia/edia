package usyd.comp3615.edia;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.*;
/**
 * Registration deals with registering the user and their details
 * @author COMP3615 Group Q edia (tomislav, michael, runi, callum, sam) 
 */
public class Registration extends Activity {

	//The items on the xml
	@SuppressWarnings("unused")
	private Button btnRegister;
	@SuppressWarnings("unused")
	private Button btnLinkToLogin;
	private EditText inputFullName;
	private EditText inputEmail;
	private EditText inputPassword;
	private EditText inputPassword2;
	private EditText inputWeight;
	private EditText inputHeight;
	
	private Spinner heightSpinner;
	private Spinner genderSpinner;
	
	private static EditText dob;
	private static String dateOfBirth;
	
	private int mYear = 1992;
	private int mMonth = 7;
	private int mDay = 15;

	private String name, email, gender, weight, height, birth, password, password2;
	//static Calendar dateOfBirth;

	//Responses
	//private static String KEY_SUCCESS = "Successful Registration, please login.";
	private static String ERROR_PASSWORD = "Passwords do not match, try again.";
	private static String ERROR_NAME = "Please enter your name.";
	private static String ERROR_EMAIL = "Please enter a valid email address";
	private static String ERROR_DOB = "Please enter your Date of Birth.";
	private static String ERROR_PASSWORD1 = "Please enter a password.";
	private static String ERROR_PASSWORD2 = "Please enter your password again.";
	private static String ERROR_WEIGHT = "Please enter your weight.";
	private static String ERROR_HEIGHT = "Please enter your height.";
	private static String ERROR_GENDER = "Please enter your gender.";
	
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
		
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			updateDisplay();
			
		}
	};
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.registration);
		// removed for lower sdk
		//getActionBar().setDisplayHomeAsUpEnabled(false);
		

		//Hide the keyboard for the birthdate field
		//dob.setInputType(InputType.TYPE_NULL);

		// Importing all assets like buttons, text fields
		inputFullName = (EditText) findViewById(R.id.registerName);
		inputEmail = (EditText) findViewById(R.id.registerEmail);
		inputWeight = (EditText) findViewById(R.id.editWeight);
		inputHeight = (EditText) findViewById(R.id.inputHeight);
		dob = (EditText) findViewById(R.id.dob);
		inputPassword = (EditText) findViewById(R.id.inputPassword);
		inputPassword2 = (EditText) findViewById(R.id.inputPassword2);
		btnRegister = (Button) findViewById(R.id.btnRegister);
		btnLinkToLogin = (Button) findViewById(R.id.btnLinkToLoginScreen);
		
		heightSpinner = (Spinner) findViewById(R.id.spinner1);
		genderSpinner = (Spinner) findViewById(R.id.genderSpinner);

		
	}
	
	public void onClick(View view) {
		//Get the fields from the screen.
		name = inputFullName.getText().toString();
		email = inputEmail.getText().toString();
		gender = genderSpinner.getItemAtPosition(genderSpinner.getSelectedItemPosition()).toString();
		weight = inputWeight.getText().toString();
		height = getHeightByM();
		birth = dob.getText().toString();
		password = inputPassword.getText().toString();
		password2 = inputPassword2.getText().toString();
		
		String data[] = {name, email, gender, weight, height, birth, password, password2};
		if (checkInput(view)) {
			Intent intent;
			
			Bundle b = new Bundle();
			b.putStringArray("data", data);
			
			intent = new Intent(Registration.this, ActivityLevel.class);
			intent.putExtras(b);
			startActivity(intent);
		}
	}
	
	private String getHeightByM() {
		if (inputHeight.getText().toString().isEmpty()) return "";
		float input = Float.parseFloat(inputHeight.getText().toString());
		String measurement = heightSpinner.getItemAtPosition(heightSpinner.getSelectedItemPosition()).toString();
		float weight = 0;
		
		if (measurement.equals("m")) {
			weight = input;
		}
		else if (measurement.equals("cm")) {
			weight = input / 100;
		}
		else {
			weight = (float) (input / 0.39370) / 100;
		}
		
		return String.valueOf(weight);
	}
			
	//Change to the login screen
	public void switchLogin(View view){
		Intent intent = new Intent(this, Login.class);
		startActivity(intent);
	}
	//Check that the password fields match
	public boolean checkPasswords(){
		return password.equals(password2);
	}
	//A helper method to put a Toast object on the screen.
	public void myToast(String msg){
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
		inputPassword.setText("");
		inputPassword2.setText("");
	}
	//A helper method to set the date on the screen after it has been picked.
	public static void setDate(int day, int month, int year){
		dateOfBirth = day + "/" + (month + 1) + "/" + year;
		dob.setText(dateOfBirth);
	}
	//Create and show the date picker dialog
	@SuppressWarnings("deprecation")
	public void showDatePickerDialog(View view) {
		// Removed for api 10
		//DialogFragment newFragment = new DatePickerFragment();
		//newFragment.show(getFragmentManager(), "datePicker");
		showDialog(0);
	}
	
	private void updateDisplay() {
	    Registration.dob.setText(
	        new StringBuilder()
	                // Month is 0 based so add 1
	                .append(mMonth + 1).append("/")
	                .append(mDay).append("/")
	                .append(mYear));
	} 
	
	@Override
	protected Dialog onCreateDialog(int id) {
	   switch (id) {
	   case 0:
	      return new DatePickerDialog(this,
	                mDateSetListener,
	                mYear, mMonth, mDay);
	   }
	   return null;
	}
	
	//Check the input from the screen
	public boolean checkInput(View view){
		if(name.isEmpty())
			myToast(ERROR_NAME);
		else if(email.isEmpty())
			myToast(ERROR_EMAIL);
		else if (gender.isEmpty() || gender.startsWith("Select"))
			myToast(ERROR_GENDER);
		else if (weight.isEmpty())
			myToast(ERROR_WEIGHT);
		else if (height.isEmpty())
			myToast(ERROR_HEIGHT);
		else if(dob.getText().toString().isEmpty())
			myToast(ERROR_DOB);
		else if(password.isEmpty())
			myToast(ERROR_PASSWORD1);
		else if(password2.isEmpty())
			myToast(ERROR_PASSWORD2);
		else if(!checkPasswords())
			myToast(ERROR_PASSWORD);
		else
			return true;
		return false;
	}
}