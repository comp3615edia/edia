package usyd.comp3615.edia;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


public class SearchAdapter extends ArrayAdapter<String>{
	
	private ArrayList<String> names;
	private ArrayList<Integer> quantities;
	private Context _context;
	private TextView searchName;
	private TextView searchResults;
	private String name;
	private int quantity;

	public SearchAdapter(Context context, ArrayList<String> names, ArrayList<Integer> quantities) {
		super(context, R.layout.search_row, names);
		this.names = names;
		this.quantities = quantities;
		this._context = context;
	}	
	
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View v = convertView;
		LayoutInflater inflater = ((Activity)_context).getLayoutInflater();
		v = inflater.inflate(R.layout.search_row, parent, false);
		
		searchName = (TextView) v.findViewById(R.id.searchName);
		searchResults = (TextView) v.findViewById(R.id.searchResults);
		
		name = names.get(position);
		quantity = quantities.get(position);
		
		System.out.println("Reached here!");
		
		searchName.setText(name);
		if(quantity == 1){
			searchResults.setText("+");
		}else{
			searchResults.setText(String.valueOf(quantity) + " results");
		}
		return v;
	}

}
