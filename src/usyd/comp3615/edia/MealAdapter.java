package usyd.comp3615.edia;

import java.text.DecimalFormat;
import java.util.ArrayList;

import usyd.comp3615.edia.db.Consumption;
import usyd.comp3615.edia.db.DatabaseHandler;
import usyd.comp3615.edia.db.FavouriteItem;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class MealAdapter extends ArrayAdapter<Consumption>{

	static class holder{
		TextView rName;
		Button quan;
		ImageButton delete;
		ImageButton fav;
	}

	private ArrayList<Consumption> meals;
	private final Context context;
	private DatabaseHandler db;
	private Consumption c;
	private String activityName;
	AlertDialog.Builder alertBuilder;

	public MealAdapter(Context context, String activityName) {
		super(context, R.layout.result_row);
		this.context = context;
		this.activityName = activityName;
		db = new DatabaseHandler(this.context);
	}

	public MealAdapter(Context context, ArrayList<Consumption> meals, String activityName) {
		super(context, R.layout.result_row, meals);
		this.context = context;
		alertBuilder = new AlertDialog.Builder(context);
		this.meals = meals;
		this.activityName = activityName;
		db = new DatabaseHandler(this.context);
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		holder h = null;
		if (v == null) {

			LayoutInflater inflater = ((Activity)context).getLayoutInflater();
			v = inflater.inflate(R.layout.result_row, parent, false);
			h = new holder();
			h.rName = (TextView) v.findViewById(R.id.mealItemName);
			h.quan = (Button) v.findViewById(R.id.mealItemQuantity);
			h.delete = (ImageButton) v.findViewById(R.id.resultDelete);
			h.fav = (ImageButton) v.findViewById(R.id.favIcon);
			v.setTag(h);
		}else{
			h = (holder) v.getTag();
		}
		
		
		c = meals.get(position);

		if (db.getFavouriteItem(c.getSurveyID()) != null){
			h.fav.setBackgroundResource(R.drawable.star_icon_selected);
		}
		else{
			h.fav.setBackgroundResource(R.drawable.star_icon);
		}
		
		h.rName.setText(c.getShortName());
		
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		SpannableString quantityText = new SpannableString(df.format(c.getQuantity()) + " grams");
		quantityText.setSpan(new UnderlineSpan(), 0, quantityText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		h.quan.setText(quantityText);
		
		h.delete.setOnClickListener(new Button.OnClickListener(){
			public void onClick(View arg0){
				Consumption d = meals.get(position);
				//delete the consumption from the database and refresh the meal list
				if(activityName == "MEALACTIVITY"){
					db.deleteConsumption(d);
				}
				meals.remove(d);
				notifyDataSetChanged();
			}
		});
		h.quan.setOnClickListener(new Button.OnClickListener(){
			public void onClick(View arg0){
				Consumption d = meals.get(position);
				Intent intent = new Intent(context, QuantitySelector.class);
				Bundle b = new Bundle();				
				b.putInt("id", d.getSurveyID());
				b.putSerializable("mode", QuantitySelector.Mode.UPDATE); //tell the quantity selector to alter the value, not add!
				b.putString("short_name", d.getShortName());
				b.putString("meal_type", d.getMealType());
				intent.putExtras(b);
				((Activity) context).startActivityForResult(intent, RequestCode.QUANTITY_SELECTION);
			}
		});

		h.fav.setOnClickListener(new Button.OnClickListener(){
			
			public void onClick(View arg0) {
				
				//final ImageButton clickedIcon = (ImageButton) arg0;
				final Consumption d = meals.get(position);
				FavouriteItem fav = db.getFavouriteItem(d.getSurveyID());
				
				if (fav != null){
					db.deleteFavourite(fav);
					Intent intent = new Intent(context,MealActivity.class);
					((Activity) context).setResult(2,intent);
					context.startActivity(intent);
					((Activity)context).finish();
				}
				else{
					//make an input box for the user to put the name in
					alertBuilder.setTitle("Add to favourites - Please provide a name for this item");
					final EditText input = new EditText(context);
					input.setInputType(InputType.TYPE_CLASS_TEXT);
					input.setTextColor(Color.WHITE);
					alertBuilder.setView(input);
					alertBuilder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface arg0, int arg1) {
							String itemName = input.getText().toString();
							ArrayList<FavouriteItem> allFavs = db.getAllFavourites();
							for (FavouriteItem fav : allFavs){
								if (fav.getName().equals(itemName)){
									Toast.makeText(context, "There is already a favourited item with this name", Toast.LENGTH_LONG).show();
									return;
								}
							}
							int itemId = d.getSurveyID();
							String description = db.getItem(itemId).getDescription();
							FavouriteItem fav = new FavouriteItem(itemId, d.getShortName(), description, itemName);
							db.addFavourite(fav);

							Intent intent = new Intent(context,MealActivity.class);
							((Activity) context).setResult(2,intent);
							context.startActivity(intent);
							((Activity)context).finish();
						}
					});

					alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
						}
					});
					
					alertBuilder.show();
				}
				
			}
			
		});
		
		return v;
	}
}