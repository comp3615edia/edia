package usyd.comp3615.edia;

import java.util.HashMap;

import usyd.comp3615.edia.db.Consumption;
import usyd.comp3615.edia.db.DatabaseHandler;
import usyd.comp3615.edia.db.Item;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.os.Bundle;
import android.view.View;
/**
 * A Quantity Selector that allows users to choose how much of an item they have eaten.
 * It extends dialog and switches to the MealActivity Activity.
 * @author sam, tomislav
 *
 */


public class QuantitySelector extends Activity {
	
	public enum Mode{
		ADD, // in add mode, the quantity selector adds the selected quantity on top of any existing consumptions
		UPDATE, // in update mode, the quantity selector changes any existing consumption to the selected result
		SELECT // in select mode, the quantity selector makes no updates to the database
	}
	
	private Item item;
	String shortName;
	String mealType;
	Mode mode;
	private float value;
	private NumberPicker picker;
	private EditText quantity;
	private ImageButton submit;
	private HashMap<String, Double> measures = new HashMap<String, Double>();
	private DatabaseHandler db;
	private Context _context = this;
	private TextView itemName;
	private TextView itemDescription;
	private Spinner spinner;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.quantity_selector);
		
		db = new DatabaseHandler(_context);
		Bundle b = this.getIntent().getExtras();
		mealType = b.getString("meal_type");
		shortName = b.getString("short_name");
		mode = (Mode) b.getSerializable("mode");
		int itemID = b.getInt("id");
		item = db.getItem(itemID);
		
		if (mealType == null || shortName == null || mode == null || item == null){
			System.err.println("Missing input - ensure meal type, short name, add and a valid item id are all provided to this activity");
			finish();
		}
		
		//Get the measurements from the database
		if (b.getBoolean("recipe") == true){
			measures.put("serves", 1.0);
		}
		else{		
			if(item!=null){
				measures.putAll(db.getMeasures(item));
			}
			//Add grams as the default measurement
			if(!measures.keySet().contains("grams")){
				//Add grams as a standard measure
				measures.put("grams", 1.0);
			}
		}
		
		itemName = (TextView) findViewById(R.id.itemName);
		itemDescription = (TextView) findViewById(R.id.itemDescription);
		quantity = (EditText) findViewById(R.id.quantityNumber);
		picker = (NumberPicker) findViewById(R.id.numberPicker1);
		
		spinner = (Spinner) findViewById(R.id.spinner1);
		submit = (ImageButton) findViewById(R.id.submitButton);
		submit.setOnClickListener(new View.OnClickListener(){
				public void onClick(View arg0) {
					//Change the consumption to the new quantity
					if(!quantity.getText().toString().equals("")){
						value = Float.parseFloat(quantity.getText().toString());
						//Check if conversion from other units is needed
						String s[] = picker.getDisplayedValues();
						String a = s[picker.getValue()];
						//Convert the chosen measure back to grams
						float gramWeight = value;
						if(!a.equals("grams")){
							gramWeight *= measures.get(a);
						}
						
						String loc = spinner.getSelectedItem().toString();
					
						Intent intent = new Intent();
						
						if (mode == Mode.SELECT){
							intent.putExtra("quantity", gramWeight);
							intent.putExtra("short_name", shortName);
							if (item != null){
								intent.putExtra("id", item.getID());
							}
						}
						else{
							//make a consumption here!!!
							Consumption existingConsumption = db.checkForConsumption(item.getID(), mealType);
							if (existingConsumption == null){
								db.addConsumption(item.getID(), shortName, gramWeight, loc, mealType);
							}
							else{
								if (mode == Mode.ADD){
									db.updateConsumption(existingConsumption.getId(), existingConsumption.getQuantity() + gramWeight);
								}
								else{
									db.updateConsumption(existingConsumption.getId(), gramWeight);
								}
							}
						}
						
						//let the calling activity (either Favourites or ItemSelection) know that the result is successful
						setResult(RESULT_OK, intent);
						finish();
					}
				}
		});
		
		String[] measuresNames = new String[measures.size()];
		measures.keySet().toArray(measuresNames);
		System.out.println(measures.size());

		int gramsIndex = 0; //if grams cannot be found for some reason, just use the first measure

		for (int i = 0; i < measuresNames.length; i++){
			//find where grams is in the list
			if (measuresNames[i].equals("grams")){
				gramsIndex = i;
				break;
			}
		}
		
		itemName.setText(shortName);
		if (b.getString("ingredients") != null){
			itemDescription.setText(b.getString("ingredients"));
		}
		else{
			itemDescription.setText(item.getDescription());
		}
		picker.setMinValue(0);
		picker.setMaxValue(measures.size()-1);
		picker.setDisplayedValues(measuresNames);
		picker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS); // stop the user from being able to type in quantities
		picker.setValue(gramsIndex);
		picker.setDisplayedValues(measuresNames);
		quantity.requestFocus();
	}
}